(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


(* ::Subsection::Closed:: *)
(*Helper functions*)


SetAttributes[dot,Orderless];


Tadpole[n_,d_,Delta_] := I/(4*Pi)^(d/2)*(-1)^n*Gamma[n-d/2]/Gamma[n]*Delta^(d/2-n)

FeynmanParam[Numerator_, D_List, alpha_List, Powers_List] := Module[{nn,norm,num,den,a,b,c,Delta},

  nn = Plus@@Powers;
  norm = Gamma[nn]/Product[alpha[[ii]]^(Powers[[ii]]-1)/Gamma[Powers[[ii]]],{ii,Range@Length@alpha}];
  den = alpha . D;
  {a,b,c} = Coefficient[den,k,{2,1,0}];
  num = Numerator /. k->k-b/a/2;
  den = Collect[den /. k->k-b/a/2,k,Factor];
  Delta = k^2-den;
  Return[norm*INT[num,nn,d,Delta,Variables[alpha]]];

]

ScalarIntegralRules = {
  INT[1,DD[l1_,m1_]]:>Tadpole[1,d,m1],
  INT[1,DD[l1_,m1_]*DD[l2_,m2_]]:>FeynmanParam[1,{l1^2-m1,l2^2-m2},{1-al[1],al[1]},{1,1}],
  INT[1,n_,d,Delta_,alpha_]:>INT[Tadpole[n,d,Delta],alpha]
};


rGamma = Gamma[1+eps]*(4*Pi)^eps;


(* g[x] indicates gamma matrix, gstr[...] indicates product of gamma matrices *)
ExpandGammaStrings = {
  g[Plus[a_,b__]]:>g[a]+g[Plus[b]],
  g[Times[a_?NumericQ,b_]]:>a*g[b],
  gstr[a1__,Plus[a_,b__],a2__]:>gstr[a1,a,a2]+gstr[a1,Plus[b],a2],
  gstr[a1__,Times[a_?NumericQ,b__],a2__]:>a*gstr[a1,b,a2],
  gstr[a1__,m,a2__]:>m*gstr[a1,a2],
  gstr[a1__,-m,a2__]:>-m*gstr[a1,a2],
  gstr[a_,g[mu[i_]],x___,y_,g[mu[i_]],b__]:>-gstr[a,g[mu[i]],x,g[mu[i]],y,b]+2*gstr[a,y,x,b],
  gstr[a_,g[mu[i_]],g[mu[i_]],b__]:>d*gstr[a,b],
  
  gstr[aa_,XX__,aa_]:>tr[XX],
  tr[]->4,
  tr[X__] :> 0 /; OddQ[Length[{X}]],
  tr[g[a1_],g[a2_]]:> 4*dot[a1,a2],
  tr[g[a1_],g[a2_],g[a3_],g[a4_]]:>4*(dot[a1,a2]*dot[a3,a4]-dot[a1,a3]*dot[a2,a4]+dot[a1,a4]*dot[a2,a3])
};


ContractRules = {
  dot[a_,b_mu]*dot[b_mu,c_]:>dot[a,c],
  dot[a_,b_mu]^2:>dot[a,a],
  dot[a_mu,a_mu]:>d,
  dot[a_,b_mu]*gstr[c__,g[b_mu],d__]:>gstr[c,g[a],d]
};


DotExpandRules = {
  dot[Plus[a_,b__],c_]:>dot[a,c]+dot[Plus[b],c],
  dot[Times[n_?NumericQ, a_],b_]:>n*dot[a,b]
};


CollectIntegralRules = {
  INT[n_,x_]*DD[y_,m_]:>INT[n*(dot[y,y]-m),x], 
  INT[n_,x_]*Power[DD[y__],a_]:>INT[n,x/DD[y]^a] /; a<0,
  INT[n_,x_]*Power[DD[y_,m_],a_]:>INT[n*(dot[y,y]-m)^a,x] /; a>0,
  dot[k,x_]*INT[n_,props_]:>INT[n*dot[k,x],props]
};


ScalelessIntegralRules = {
  INT[1,DD[k+p1,0] DD[k-p2,0]]->INT[1,DD[k,0] DD[k-p1-p2,0]], (* integral symmetry *)
  INT[_,DD[k,0] DD[k-p2,0]]->0,
  INT[_,DD[k,0] DD[k+p1,0]]->0,
  INT[_,DD[k,0]]->0,
  (*INT[dot[k,p_],DD[k,m_]]:>0 /; !MatchQ[p,k],*) (* not scaleless but zero due to parity odd integrand *)
  INT[1,1]->0
};


(* ::Subsection::Closed:: *)
(*Load Passarino-Veltman reduction rules*)


Get["PVreduction-bubble.m"];


Get["PVreduction-triangle.m"];


(* ::Subsection::Closed:: *)
(*Electron self energy: mass and electron wave-function counter-terms*)


Num = (-I*eR*MuR^eps)^2*(I*gstr[bb,g[mu[1]],g[-k+p]+m,g[mu[2]],aa])*(-I*dot[mu[1],mu[2]])
Num = Expand[Num] //. ContractRules
Num  = Collect[Num //. ExpandGammaStrings,_gstr,Factor]


SEgraph = Expand[Num*INT[1,1]/DD[k,0]/DD[k-p,m^2]];
SEgraph = SEgraph  /. gstr[a__,g[k],b__]:>dot[k,mu[1]]*gstr[a,g[mu[1]],b] //. CollectIntegralRules;
SEgraph = Collect[SEgraph,_INT,Factor]


SEgraph = Collect[SEgraph /. ExpandToTensorBasis[bubble] //. ContractRules,_gstr,Factor]


SEgraph = Collect[SEgraph /. TensoReductionCoefficients[bubble] /. ScalelessIntegralRules,{_gstr,_INT},Factor]


SEgraph = Collect[SEgraph //. ScalarIntegralRules /. d->4-2*eps // Simplify,{_INT},Factor];
SEgraph = SEgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
(* expand in eps in two steps - since the Gamma[eps] pole has been idenfiified in the integral coefficient we expand the integral to O(eps)  *)
SEgraph = SEgraph  /. INT[x_,y_]:>INT[Normal@Series[x,{eps,0,1}],y];
SEgraph = SEgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
SEgraph = Collect[SEgraph,_INT,Factor]


(* now integrate. use symbols msq and psq and integrate in the Euclidean region where psq<0 *)
(* This is to simplify the expressions and ensure factors of I*Pi do not appear *)
SEgraphInt = SEgraph /. {
  INT[1,{al[1]}]->Integrate[1,{al[1],0,1}],
  INT[Log[(m^2+p^2 (-1+al[1])) al[1]],{al[1]}]->
      Integrate[Log[(msq+psq (-1+al[1])) al[1]],{al[1],0,1},Assumptions->\!\(\*
TagBox[
StyleBox[
RowBox[{"Element", "[", 
RowBox[{"msq", ",", "Reals"}], "]"}],
ShowSpecialCharacters->False,
ShowStringCharacters->True,
NumberMarks->True],
FullForm]\)&&\!\(\*
TagBox[
StyleBox[
RowBox[{"Element", "[", 
RowBox[{"psq", ",", "Reals"}], "]"}],
ShowSpecialCharacters->False,
ShowStringCharacters->True,
NumberMarks->True],
FullForm]\)&&psq<msq&&msq>0]
} /. p^2->psq /. m^2->msq /. dot[p,p]->psq;
SEgraphInt = Normal@Series[SEgraphInt/rGamma,{eps,0,0}];
SEgraphInt = Collect[SEgraphInt,{eps,_gstr},Simplify];
SEgraphInt = SEgraphInt/. Log[MuR]->(Log[MuRsq/msq]+Log[msq])/2 /.  Log[msq-psq]->Log[1-psq/msq]+Log[msq] /. eR->Sqrt[\[Alpha]*4*Pi];
SEgraphInt = Collect[SEgraphInt,{eps,_gstr,_Log},Factor]


SEcounterterm = I*((gstr[bb,g[p],aa]-m*gstr[bb,aa])*dpsi-m*dm*gstr[bb,aa]);


SEfinite = SEgraphInt+SEcounterterm /. {
  dm->\[Alpha]/(4*Pi)*(-3/eps+kappam),
  dpsi->\[Alpha]/(4*Pi)*(-1/eps+kappapsi)
};
Collect[SEfinite/(\[Alpha]/(4*Pi)/(-I)),{eps,_gstr,_Log},Factor]


(* on-shell renormalisation conditions *)
Normal@Series[SEfinite /. gstr[bb,g[p],aa]->m*gstr[bb,aa],{psq,msq,0}]//Factor
Normal@Series[D[SEfinite,gstr[bb,g[p],aa]] /. gstr[bb,g[p],aa]->m*gstr[bb,aa],{psq,msq,0}]//Factor


(* on-shell renormalisation conditions *)
tmp = SEfinite /. gstr[bb,g[p],aa]->sl[p] /. psq->sl[p]^2 /. gstr[bb,aa]->1 /. msq->m^2;
Normal@Series[tmp,{sl[p],m,0}]
(Normal@Series[tmp,{sl[p],m,1}]-%)/(sl[p]-m)
(* NB in the last step we see a problem with the exchange of limits (eps\[Rule]0 and sl[p]\[Rule]m) - the second condition generates an (IR) divergent expression *)
(* one must take the limit before integration to obtain the correct result *)


Normal@Series[SEfinite,{psq,msq,1}]


(* ::Subsection:: *)
(*Photon Vacuum polarisation: photon wave-function counter-term*)


Num = -(-I*eR*MuR^eps)^2*(I^2*gstr[aa,g[mu[1]],g[k]+m,g[mu[2]],g[k-p]+m,aa])
Num = Expand[Num] //. ContractRules
Num = Num //. ExpandGammaStrings //. DotExpandRules
Num  = Collect[Num /. dot[k,k]->(DD[k,m^2]+m^2),_gstr,Factor]


VPgraph = Expand[Num*INT[1,1]/DD[k,m^2]/DD[k-p,m^2]];
VPgraph = VPgraph //. CollectIntegralRules;
VPgraph = Collect[VPgraph,_INT,Factor]


VPgraph = Collect[VPgraph /. ExpandToTensorBasis[bubble] //. ContractRules,_gstr,Factor]


VPgraph = Collect[VPgraph /. TensoReductionCoefficients[bubble] /. ScalelessIntegralRules /. INT[1,DD[k-p,m^2]]->INT[1,DD[k,m^2]],{_INT},Factor]


Collect[Coefficient[VPgraph/(-eR^2*MuR^(2*eps)),dot[mu[1],mu[2]]],_INT,Factor]
Collect[Coefficient[VPgraph/(-eR^2*MuR^(2*eps)),dot[p,mu[1]]*dot[p,mu[2]]],_INT,Factor]
%-(-1/dot[p,p]*%%) // Factor


VPgraph = Collect[VPgraph //. ScalarIntegralRules /. d->4-2*eps // Simplify,{_INT},Factor];
VPgraph = VPgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
(* expand in eps in two steps - since the Gamma[eps] pole has been idenfiified in the integral coefficient we expand the integral to O(eps)  *)
VPgraph = VPgraph  /. INT[x_,y_]:>INT[Normal@Series[x,{eps,0,1}],y];
VPgraph = VPgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
VPgraph = Collect[VPgraph,_INT,Factor]


(* now integrate. use symbols msq and psq and integrate in the Euclidean region where psq<0 *)
(* This is to simplify the expressions and ensure factors of I*Pi do not appear *)
VPgraphInt = VPgraph  /. p^2->psq /. m^2->msq /. dot[p,p]->psq //. {
  INT[Log[msq+psq (-1+al[1]) al[1]],{al[1]}]->INT[Log[1+psq/msq (-1+al[1]) al[1]],{al[1]}]+INT[1,{al[1]}]*Log[msq],
  INT[1,{al[1]}]->Integrate[1,{al[1],0,1}],
  INT[Log[1+psq/msq (-1+al[1]) al[1]],{al[1]}]->-2+beta[psq,msq]*Log[-beta[1,psq,msq]/beta[-1,psq,msq]]
};
VPgraphInt = Normal@Series[VPgraphInt/rGamma,{eps,0,0}];
VPgraphInt = Collect[VPgraphInt,{eps,_dot},Simplify];
VPgraphInt = VPgraphInt/. Log[MuR]->(Log[MuRsq/msq]+Log[msq])/2 /.  Log[msq-psq]->Log[1-psq/msq]+Log[msq] /. eR->Sqrt[\[Alpha]*4*Pi];
VPgraphInt = Collect[Factor[VPgraphInt/(-psq*dot[mu[1],mu[2]]+dot[p,mu[1]]*dot[p,mu[2]])],{eps,_dot,_Log},Factor]


VPcounterterm = I*(-psq*dot[mu[1],mu[2]]+dot[p,mu[1]]*dot[p,mu[2]])*dA


VPfinite = VPgraphInt + VPcounterterm/(-psq*dot[mu[1],mu[2]]+dot[p,mu[1]]*dot[p,mu[2]]) /. {
  dA->\[Alpha]/(4*Pi)*(-4/3/eps+kappaA)
};
VPfinite = Collect[VPfinite/(\[Alpha]/(4*Pi)/(-I)),{eps,_gstr,_Log},Factor]


Normal@Series[VPfinite/. beta[a_,s_,m2_]:>1/2(1+a*beta[s,m2]) /. beta[s_,m2_]:>Sqrt[1-4*m2/s],{psq,0,0}]
onshellsol = Solve[%==0,kappaA][[1]]


Collect[VPfinite /. onshellsol,_Log]


Normal@Series[VPfinite /. onshellsol /.  beta[a_,s_,m2_]:>1/2(1+a*beta[s,m2]) /. beta[s_,m2_]:>Sqrt[1-4*m2/s],{psq,0,1}]


Normal@Series[beta[psq,msq] Log[-(beta[1,psq,msq]/beta[-1,psq,msq])] /.  beta[a_,s_,m2_]:>1/2(1+a*beta[s,m2]) /. beta[s_,m2_]:>Sqrt[1-4*m2/s],{psq,0,1}]


VPgraphInt + VPcounterterm/(-psq*dot[mu[1],mu[2]]+dot[p,mu[1]]*dot[p,mu[2]])
Normal@Series[% /.  beta[a_,s_,m2_]:>1/2(1+a*beta[s,m2]) /. beta[s_,m2_]:>Sqrt[1-4*m2/s],{psq,0,0}]
Solve[%==0,dA][[1]] // Simplify
dA->\[Alpha]/(4*Pi)*(-4/3/eps+kappaA) /. onshellsol // Simplify


VPgraphInt + VPcounterterm/(-psq*dot[mu[1],mu[2]]+dot[p,mu[1]]*dot[p,mu[2]]) /. dA->\[Alpha]/(4*Pi)*(-4/3/eps+kappaA) /. onshellsol;
Collect[%,_Log,Factor]


VPgraphInt - (-I)*(\[Alpha]/4/Pi)*4/3*( -1/eps*(1+eps*Log[MuRsq/msq]-eps/3) - (1+2*msq/psq)*(2-beta[psq,msq]*Log[-beta[1,psq,msq]/beta[-1,psq,msq]]))
Collect[%,_Log,Factor]


(* ::Subsection::Closed:: *)
(*Vertex correction: vertex counter-term, \[Delta]1*)


Num = (-I*eR*MuR^eps)^3*I^2*gstr[vb2,g[mu[2]],-g[k+p2]+m,g[mu[1]],-g[k-p1]+m,g[mu[3]],u1]*(-I*dot[mu[2],mu[3]])
Num = Expand[Num] //. ContractRules;
Num = Num //. ExpandGammaStrings //. DotExpandRules;
Num = Num //. {
  gstr[a_,g[mu[2]],x___,y_,g[mu[2]],b__]:>-gstr[a,g[mu[2]],x,g[mu[2]],y,b]+2*gstr[a,y,x,b],
  gstr[a__,g[x_],g[x_],b__]:>dot[x,x]*gstr[a,b],
  gstr[a_,g[mu[2]],g[mu[2]],b__]:>d*gstr[a,b],
  
  gstr[vb2,g[p2],a__]:>-m*gstr[vb2,a],
  gstr[a__,g[p1],u1]:>m*gstr[a,u1],
  gstr[vb2,x___,g[y_],g[p2],b__]:>-gstr[vb2,x,g[p2],g[y],b]+2*dot[y,p2]*gstr[vb2,x,b],
  gstr[b__,g[p1],g[y_],x___,u1]:>-gstr[b,g[y],g[p1],x,u1]+2*dot[y,p1]*gstr[b,x,u1],
  gstr[a__,g[mu[1]],g[k],b__]:>-gstr[a,g[k],g[mu[1]],b]+2*dot[k,mu[1]]*gstr[a,b]
};
Num  = Collect[Num /. gstr[vb2,g[k],u1]->gstr[vb2,g[mu[2]],u1]*dot[k,mu[2]]/. dot[k,k]->DD[k,0],_gstr,Factor]


VRTgraph = Expand[Num*INT[1,1]/DD[k,0]/DD[k-p1,m^2]/DD[k+p2,m^2]];
VRTgraph = VRTgraph //. CollectIntegralRules;
VRTgraph = Collect[VRTgraph,_INT,Factor]


VRTgraph /. m->0


VRTgraph = Collect[Expand[VRTgraph /. ExpandToTensorBasis[triangle]]//. DotExpandRules //. ContractRules //. DotExpandRules,_gstr,Factor];
VRTgraph = Collect[VRTgraph /. {
  gstr[vb2,g[p2],a__]:>-m*gstr[vb2,a],
  gstr[a__,g[p1],u1]:>m*gstr[a,u1]
},_gstr,Factor]


VRTgraph = Collect[VRTgraph //. TensoReductionCoefficients[triangle] //. TensoReductionCoefficients[bubble] /. ScalelessIntegralRules,{_INT},Factor];
VRTgraph = VRTgraph /. {
  INT[1,DD[k-p,m^2]]->INT[1,DD[k,m^2]],
  INT[1,DD[k,0] DD[k+p2,m^2]]->INT[1,DD[k,0] DD[k-p1,m^2]],
  INT[1,DD[k-p1,m^2] DD[k+p2,m^2]]->INT[1,DD[k,m^2] DD[k+p1+p2,m^2]]
};
VRTgraph = VRTgraph //. DotExpandRules /. a0[0]->0 /. a0[m^2]->INT[1,DD[k,m^2]];
VRTgraph = VRTgraph /. dot[p1,p1]->m^2 /. dot[p2,p2]->m^2;
VRTgraph = VRTgraph /. dot[p1,mu[1]]->dot[p2,mu[1]]+1/gstr[vb2,u1]*(2*m*gstr[vb2,g[mu[1]],u1]-I*gstr[vb2,sig[mu[1],p12],u1]);
Collect[VRTgraph,{_gstr,_INT},Factor]


Collect[Coefficient[VRTgraph,gstr[vb2,g[mu[1]],u1]] /. m->0 /. INT[1,DD[k,0]]->0 /. INT[1,DD[k,0] DD[k-p1,0]]->0,_INT,Factor]


VRTgraph = Collect[VRTgraph //. ScalarIntegralRules /. d->4-2*eps// Simplify,{_INT},Factor]  /. (p1+p2)^2->Qsq /. p1^2->m^2;
VRTgraph = VRTgraph /. INT[n_,args__]:>INT[Simplify[n],args];
VRTgraph = VRTgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
(* expand in eps in two steps - since the Gamma[eps] pole has been idenfiified in the integral coefficient we expand the integral to O(eps)  *)
VRTgraph = VRTgraph  /. INT[x_,y_]:>INT[Normal@Series[x,{eps,0,1}],y];
VRTgraph = VRTgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
VRTgraph = Collect[VRTgraph,_INT,Factor]


(* now integrate. use symbols msq and psq and integrate in the Euclidean region where psq<0 *)
(* This is to simplify the expressions and ensure factors of I*Pi do not appear *)
VRTgraphInt = VRTgraph /. Q^2->Qsq /. m^2->msq //. {
  INT[Log[msq+Qsq (-1+al[1]) al[1]],{al[1]}]->INT[Log[1+Qsq/msq (-1+al[1]) al[1]],{al[1]}]+INT[1,{al[1]}]*Log[msq],
  INT[1,{al[1]}]->Integrate[1,{al[1],0,1}],
  INT[Log[1+Qsq/msq (-1+al[1]) al[1]],{al[1]}]->-2+beta[Qsq,msq]*Log[-beta[1,Qsq,msq]/beta[-1,Qsq,msq]],
  INT[Log[msq*al[1]^2],{al[1]}]->Integrate[Log[msq*al[1]^2],{al[1],0,1}]
};
VRTgraphInt = Normal@Series[VRTgraphInt/rGamma,{eps,0,0}];
VRTgraphInt = Collect[VRTgraphInt,{eps,_gstr},Simplify];
VRTgraphInt = Collect[VRTgraphInt/. Log[MuR]->(Log[MuRsq/msq]+Log[msq])/2 /. eR->Sqrt[\[Alpha]*4*Pi],{eps,_gstr,_Log,_INT},Factor] /. m^2->msq


VRTcounterterm = -I*eR*MuR^eps*gstr[vb2,g[mu[1]],u1]*d1 /. eR->Sqrt[\[Alpha]*4*Pi]


VRTgraphInt+VRTcounterterm /. d1->\[Alpha]/(4*Pi)*(-1/eps+kappa1);
VRTfinite = Collect[Normal@Series[%/(-I*Sqrt[\[Alpha]*4*Pi]*MuR^eps),{eps,0,0}],{eps,_gstr,_Log,_INT},Factor];
VRTfinite = Collect[VRTfinite //. {
Log[MuR]->(Log[MuRsq/msq]+Log[msq])/2,
dot[p1,p2]->(Qsq-2*msq)/2,
INT[1,DD[k,0] DD[k-p1,msq] DD[k+p2,msq]]->-I/(4*Pi)^2*I3hat[Qsq,msq]
},{eps,_gstr,_Log,_I3hat,_beta},Simplify@Factor[# /. Qsq->1/(1-beta[Qsq,msq]^2)*msq*4]&]


deltaF1 = Collect[Coefficient[VRTfinite*4*Pi/\[Alpha],gstr[vb2,g[mu[1]],u1]],{_Log,_I3hat},Factor] /. 1/(-1+beta[Qsq,msq])/(1+beta[Qsq,msq])->-Qsq/4/msq


deltaF2 = Coefficient[VRTfinite*4*Pi/\[Alpha],gstr[vb2,sig[mu[1],p12],u1]]*(-I*2*m) /. m^2->msq /. (-1+beta[Qsq,msq]) (1+beta[Qsq,msq])->-4*msq/Qsq


deltaF1 /. beta[a_,s_,m2_]:>1/2(1+a*beta[s,m2]) /. beta[s_,m2_]:>Sqrt[1-4*m2/s];
Solve[Collect[Normal@Series[%,{Qsq,0,0}],{eps,_gstr,_INT},Factor]==0,kappa1]
deltaF1 /. %[[1]]
Collect[Normal@Series[% /. beta[a_,s_,m2_]:>1/2(1+a*beta[s,m2]) /. beta[s_,m2_]:>Sqrt[1-4*m2/s],{Qsq,0,1}],{eps,_gstr,_INT},Factor]


deltaF2 /. beta[a_,s_,m2_]:>1/2(1+a*beta[s,m2]) /. beta[s_,m2_]:>Sqrt[1-4*m2/s];
Collect[Normal@Series[%,{Qsq,0,0}],{eps,_gstr,_INT},Factor]
