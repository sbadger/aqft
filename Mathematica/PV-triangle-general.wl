(* ::Package:: *)

(* Rank one and two and three triangle tensor reduction problem *)


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


(* this file is generated by "PV-bubble-general.wl" *)
Get["PVreduction-bubble.m"]


(*

Representation of lorentz indices, vectors and momenta

g^{mu,nu} = dot[mu,nu]
p^mu = dot[p,mu]
p1.p2 = dot[p1,p2]

*)

SetAttributes[dot,Orderless];

ContractRules = {
  dot[a_,b_mu]*dot[b_mu,c_]:>dot[a,c],
  dot[a_,b_mu]^2:>dot[a,a],
  dot[a_mu,a_mu]:>d
};

DotExpandRules = {
  dot[Plus[a_,b__],c_]:>dot[a,c]+dot[Plus[b],c],
  dot[Times[n_?NumericQ, a_],b_]:>n*dot[a,b]
};


Num[mu1_] := dot[k,mu1]
TensorBasis[mu1_,p1_,p2_] := {dot[p1,mu1],dot[p2,mu1]}

Num[mu1_,mu2_] := dot[k,mu1]*dot[k,mu2]
TensorBasis[mu1_,mu2_,p1_,p2_] := {dot[mu1,mu2],dot[p1,mu1]*dot[p1,mu2],dot[p2,mu1]*dot[p2,mu2],1/2*(dot[p1,mu1]*dot[p2,mu2]+dot[p2,mu1]*dot[p1,mu2])}

Num[mu1_,mu2_,mu3_] := dot[k,mu1]*dot[k,mu2]*dot[k,mu3]
TensorBasis[mu1_,mu2_,mu3_,p1_,p2_] := {
dot[mu1,mu2]*dot[p1,mu3]+dot[mu2,mu3]*dot[p1,mu1]+dot[mu3,mu1]*dot[p1,mu2],
dot[mu1,mu2]*dot[p2,mu3]+dot[mu2,mu3]*dot[p2,mu1]+dot[mu3,mu1]*dot[p2,mu2],
dot[p1,mu1]*dot[p1,mu2]*dot[p1,mu3],
dot[p2,mu1]*dot[p2,mu2]*dot[p2,mu3],
dot[p1,mu1]*dot[p1,mu2]*dot[p2,mu3]+dot[p1,mu1]*dot[p2,mu2]*dot[p1,mu3]+dot[p2,mu1]*dot[p1,mu2]*dot[p1,mu3],
dot[p1,mu1]*dot[p2,mu2]*dot[p2,mu3]+dot[p2,mu1]*dot[p1,mu2]*dot[p2,mu3]+dot[p2,mu1]*dot[p2,mu2]*dot[p1,mu3]
}


(* rewrite dot products as propagators *)
(* Massless triangle with one-leg off-shell p1^2=p2^2=0 *)
ToPropagators = {
  dot[k,p2]->1/2*(DD[k,m1]-DD[k-p2,m2]+dot[p2,p2]+m1-m2),
  dot[k,p1]->1/2*(DD[k+p1,m3]-DD[k,m1]-dot[p1,p1]+m3-m1),
  dot[k,k]->DD[k,0]
}


(* check we did it right *)
ToPropagators /. DD[k_,m_]:>k^2-m /. dot[x_,y_]:>x*y /. Rule->Equal // Expand


kinematics = {
  dot[p1,p1]->p1sq, dot[p2,p2]->p2sq, dot[p1,p2]->1/2*(p3sq-p1sq-p2sq)
}


(* rules for removing scalesless integrals *)
ScalelessIntegralRules = {
  INT[1,DD[k+p1,0] DD[k-p2,0]]->INT[1,DD[k,0] DD[k-p1-p2,0]], (* integral symmetry *)
  INT[_,DD[k,0] DD[k-p2,0]]->0,
  INT[_,DD[k,0] DD[k+p1,0]]->0,
  INT[_,DD[k,0]]->0,
  (*INT[dot[k,p_],DD[k,m_]]:>0 /; !MatchQ[p,k],*) (* not scaleless but zero due to parity odd integrand *)
  INT[1,1]->0
};
(* collect numerators and propagators into integral wrappers *)
CollectIntegralRules = {
  INT[n_,x_]*DD[y_,m_]:>INT[n*(dot[y,y]-m),x], 
  INT[n_,x_]*Power[DD[y__],a_]:>INT[n,x/DD[y]^a] /; a<0,
  INT[n_,x_]*Power[DD[y_,m_],a_]:>INT[n*(dot[y,y]-m)^a,x] /; a>0
};
(* expand integrals into individual tnesor integrals *)
ExpandIntegralNumeratorRules = {
  INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c],
  INT[Times[n_?NumericQ, a_],b_]:>n*INT[a,b],
  INT[Times[n_,a__],b_]:>n*INT[a,b] /; FreeQ[n,k],
  INT[n_,b_]:>n*INT[1,b] /; FreeQ[n,k]&&!MatchQ[n,1]
};


Basis = TensorBasis[mu[1],p1,p2];
FormFactors = {c1,c2};
Integrand = Num[mu[1]]/DD[k,m1]/DD[k-p2,m2]/DD[k+p1,m3]

(* calculate the contractions with the tensor integrals in terms of scalar integrals *)
Expand[Integrand*Basis] //. ContractRules /. ToPropagators
(* now collect into integrals, expand dot products in numerator and reduce tadpoles *)
Expand[%*INT[1,1]];
% //. CollectIntegralRules;
% /. INT[n_,DD[k-p2,mm1_]*DD[k+p1,mm2_]]:>INT[n /. k->k-p1,DD[k,mm2]*DD[k-p1-p2,mm1]];
% /. INT[n_,props_]:>INT[Expand[n  //. DotExpandRules],props];
% //. ExpandIntegralNumeratorRules;
(*assign as Left Hand Side (LHS) of the equations*)
LHS = % //. ScalelessIntegralRules  /. kinematics;
LHS = LHS /. {
INT[1,DD[k,m1_] DD[Plus[k,pp__],m2_]*DD[Plus[k,qq__],m3_]]:>c0[Plus[pp],-Plus[qq],m1,m3,m2],
INT[1,DD[k,m1_] DD[Plus[k,pp__],m2_]]:>b0[-Plus[pp],m1,m2]
};

(* now the Right Hand Side (RHS) of the equations  *)
RHS = Outer[Times,Basis,Basis] . FormFactors //. ContractRules /. kinematics;

(* now construct equations and solve linear system *)
eqs = Equal@@@Transpose@{LHS,RHS};
r1sol = Solve[eqs,FormFactors][[1]] /. Rule[a_,b_]:>Rule[a,Collect[b,{_b0,_c0},Factor]]


Basis = TensorBasis[mu[1],mu[2],p1,p2];
FormFactors = {c00,c11,c22,c12};
Integrand = Num[mu[1],mu[2]]/DD[k,m1]/DD[k-p2,m2]/DD[k+p1,m3]

(* calculate the contractions with the tensor integrals in terms of scalar integrals *)
Expand[Integrand*Basis] //. ContractRules /. ToPropagators
(* now collect into integrals, expand dot products in numerator and reduce tadpoles *)
Expand[%*INT[1,1]];
% //. CollectIntegralRules;
% /. INT[n_,DD[k-p2,mm_]]:>INT[n /. k->k+p2,DD[k,mm]];
% /. INT[n_,DD[k+p1,mm_]]:>INT[n /. k->k-p1,DD[k,mm]];
% /. INT[n_,DD[k-p2,mm1_]*DD[k+p1,mm2_]]:>INT[n /. k->k-p1,DD[k,mm2]*DD[k-p1-p2,mm1]];
% /. INT[n_,props_]:>INT[n  //. DotExpandRules,props];
% //. ExpandIntegralNumeratorRules;
(* write tensor bubbles in terms of bubble form factors *)
% /. INT[n_,DD[k-p2,mm1_]*DD[k+p1,mm2_]]:>INT[n /. k->k-p1,DD[k,mm2]*DD[k-p1-p2,mm1]];
% /. INT[dot[k,k],DD[k,mm1_] DD[k-p1-p2,mm2_]]:>INT[1,DD[k-p1-p2,mm2]]+mm1*INT[1,DD[k,mm1] DD[k-p1-p2,mm2]] /. INT[1,DD[k-p1-p2,m_]]:>INT[1,DD[k,m]];
% /. INT[dot[k,k],DD[k,m1]*DD[k-p2,m2]*DD[k+p1,m3]]->INT[1,DD[k-p2,m2]*DD[k+p1,m3]]+m1*INT[1,DD[k,m1]*DD[k-p2,m2]*DD[k+p1,m3]];
% /. INT[n_,DD[k-p2,mm1_]*DD[k+p1,mm2_]]:>INT[n /. k->k-p1,DD[k,mm2]*DD[k-p1-p2,mm1]];
% //. ScalelessIntegralRules /. kinematics;

% //. {
  INT[dot[k,k],DD[k,mm1_] DD[Plus[k,p__],mm2_]]:>INT[1,DD[Plus[k,p],mm2]]+mm1*INT[1,DD[k,mm1] DD[Plus[k,p],mm2]],
  INT[dot[k,k]*X_,DD[k,mm1_] DD[Plus[k,p__],mm2_]]:>INT[X,DD[Plus[k,p],mm2]]+mm1*INT[X,DD[k,mm1] DD[Plus[k,p],mm2]],
  INT[dot[k,k]^2,DD[k,mm1_] DD[Plus[k,p__],mm2_]]:>INT[dot[k,k],DD[Plus[k,p],mm2]]+mm1*INT[dot[k,k],DD[k,mm1] DD[Plus[k,p],mm2]],
  INT[n_,DD[k-p1-p2,m_]]:>INT[Expand[n   /. k->k+p1+p2 //. DotExpandRules //. kinematics],DD[k,m]],
  INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c],
  INT[Times[n_?NumericQ, a_],b_]:>n*INT[a,b], INT[n_,b_]:>n*INT[1,b] /; FreeQ[n,k]&&!MatchQ[n,1],
  INT[1,DD[k,0]]->0,
  INT[dot[k,k],DD[k,0]]->0,
  INT[dot[k,mu_],DD[k,0]]->0
};
% /. INT[n_,DD[k-p2,mm_]]:>INT[n /. k->k+p2,DD[k,mm]];
tmp = % /. INT[n_,DD[k+p1,mm_]]:>INT[n /. k->k-p1,DD[k,mm]];

LHS = %  /. ExpandToTensorBasis[bubble] //. DotExpandRules /. kinematics;

(* write all integrals in terms of bubble form factors... *)
LHS = LHS  /. {
INT[1,DD[k,m1_]]:>a0[m1],
INT[1,DD[k,m1_] DD[Plus[k,pp2__],m2_] DD[Plus[k,pp1__],m3_]]:>c0[Plus[pp1],-Plus[pp2],m1,m2,m3],
INT[1,DD[k,m1_] DD[Plus[k,pp__],m2_]]:>b0[-Plus[pp],m1,m2]
}

(* now the Right Hand Side (RHS) of the equations  *)
RHS = Expand[Outer[Times,Basis,Basis] . FormFactors] //. ContractRules /. kinematics;

(* now construct equations and solve linear system *)
eqs = Equal@@@Transpose@{LHS,RHS};
r2sol = Solve[eqs,FormFactors][[1]] /. Rule[a_,b_]:>Rule[a,Collect[b,{_b0,_b1,_c0,_a0},Factor]];


c00*2*(d-2) - (2*m1*c0[p1,p2,m1,m2,m3] - (-p1sq-m1+m3)*c1 - (p2sq-m2+m1)*c2)  - INT[1,DD[k,m3] DD[k-p1-p2,m2]]/. r2sol /. r1sol;
% //. TensoReductionCoefficients[bubble] /. a0[m_]:>INT[1,DD[k,m]] //. DotExpandRules/. dot[p1,p1]->p1sq /. dot[p2,p2]->p2sq /. dot[p1,p2]->1/2*(p3sq-p1sq-p2sq);
Collect[%,{_INT,_c0},Factor]


Basis = TensorBasis[mu[1],mu[2],mu[3],p1,p2];
FormFactors = {c001,c002,c111,c222,c112,c122};
Integrand = Num[mu[1],mu[2],mu[3]]/DD[k,m1]/DD[k-p2,m2]/DD[k+p1,m3]

(* calculate the contractions with the tensor integrals in terms of scalar integrals *)
Expand[Integrand*Basis] //. ContractRules /. ToPropagators
(* now collect into integrals, expand dot products in numerator and reduce tadpoles *)
Expand[%*INT[1,1]];
% //. CollectIntegralRules;
% /. INT[n_,DD[k-p2,mm_]]:>INT[n /. k->k+p2,DD[k,mm]];
% /. INT[n_,DD[k+p1,mm_]]:>INT[n /. k->k-p1,DD[k,mm]];
% /. INT[n_,DD[k-p2,mm1_]*DD[k+p1,mm2_]]:>INT[n /. k->k-p1,DD[k,mm1]*DD[k-p1-p2,mm2]];
% /. INT[n_,props_]:>INT[Expand[n  //. DotExpandRules],props];
% //. ExpandIntegralNumeratorRules;

(* write tensor bubbles in terms of bubble form factors *)
% /. INT[n_,DD[k-p2,mm1_]*DD[k+p1,mm2_]]:>INT[n /. k->k-p1,DD[k,mm2]*DD[k-p1-p2,mm1]];
% /. INT[dot[k,k],DD[k,m1_] DD[k-p1-p2,m2_]]:>INT[1,DD[k-p1-p2,m2]]+m1*INT[1,DD[k,m1] DD[k-p1-p2,m2]] /. INT[1,DD[k-p1-p2,m_]]:>INT[1,DD[k,m]];
% /. INT[dot[k,k],DD[k,m1]*DD[k-p2,m2]*DD[k+p1,m3]]->INT[1,DD[k-p2,m2]*DD[k+p1,m3]]+m1*INT[1,DD[k,m1]*DD[k-p2,m2]*DD[k+p1,m3]];
% /. INT[n_,DD[k-p2,mm1_]*DD[k+p1,mm2_]]:>INT[n /. k->k-p1,DD[k,mm1]*DD[k-p1-p2,mm2]];
% //. ScalelessIntegralRules /. kinematics;
% //. ExpandIntegralNumeratorRules;

% //. {
  INT[dot[k,k],DD[k,m1_] DD[Plus[k,p__],m2_]]:>INT[1,DD[Plus[k,p],m2]]+m1*INT[1,DD[k,m1] DD[Plus[k,p],m2]],
  INT[dot[k,k]*X_,DD[k,m1_] DD[Plus[k,p__],m2_]]:>INT[X,DD[Plus[k,p],m2]]+m1*INT[X,DD[k,m1] DD[Plus[k,p],m2]],
  INT[dot[k,k]^2,DD[k,m1_] DD[Plus[k,p__],m2_]]:>INT[dot[k,k],DD[Plus[k,p],m2]]+m1*INT[dot[k,k],DD[k,m1] DD[Plus[k,p],m2]],
  INT[n_,DD[k-p1-p2,m_]]:>INT[Expand[n   /. k->k+p1+p2 //. DotExpandRules //. kinematics],DD[k,m]],
  INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c],
  INT[Times[n_?NumericQ, a_],b_]:>n*INT[a,b], INT[n_,b_]:>n*INT[1,b] /; FreeQ[n,k]&&!MatchQ[n,1],
  INT[1,DD[k,0]]->0,
  INT[dot[k,k],DD[k,0]]->0,
  INT[dot[k,mu_],DD[k,0]]->0
};
% /. INT[n_,DD[k-p2,mm_]]:>INT[n /. k->k+p2,DD[k,mm]];
% /. INT[n_,DD[k+p1,mm_]]:>INT[n /. k->k-p1,DD[k,mm]];
% /. INT[n_,props_]:>INT[Expand[n  //. DotExpandRules],props];
% //. ExpandIntegralNumeratorRules;
% /. INT[dot[k,k],DD[k,m1_]]:>INT[1,1]+m1*INT[1,DD[k,m1]];
% //. ScalelessIntegralRules /. kinematics;

LHS = %  /. ExpandToTensorBasis[bubble] //. DotExpandRules /. kinematics;

(* write all integrals in terms of bubble form factors... *)
LHS = LHS  /. {
INT[1,DD[k,m1_]]:>a0[m1],
INT[1,DD[k,m1_] DD[Plus[k,pp__],m2_]DD[Plus[k,qq__],m3_]]:>c0[Plus[pp],-Plus[qq],m1,m3,m2],
INT[1,DD[k,m1_] DD[Plus[k,pp__],m2_]]:>b0[-Plus[pp],m1,m2]
} // Collect[#,{_a0,_b0,_b1,_b00,_b11,_c0},Factor]&;

(* now the Right Hand Side (RHS) of the equations  *)
RHS = Expand[Outer[Times,Basis,Basis] . FormFactors] //. ContractRules /. kinematics

(* now construct equations and solve linear system *)
(* in this case Mathematica's 'Solve' struggled for some reason so the linear algebra is done by hand *)
(* put RHS in matrix form *)
Coefficient[#,FormFactors]&/@RHS;
(* find inverse of the matrix *)
Factor[Inverse[Simplify[%]]];
r3sol = Rule@@@Transpose@{FormFactors,% . LHS} /. Rule[a_,b_]:>Rule[a,Collect[b,{_a0,_b0,_b1,_b00,_b11,_c0},Factor]];


r3sol /. m1->0 /. m2->0 /. m3->0 /. p1sq->0 /. p2sq->0 /. Rule[a_,b_]:>Rule[a,Collect[b,{_a0,_b0,_b1,_b00,_b11,_c0},Factor]]
%[[;;,2]]/INT[1,DD[k,0] DD[k-p1-p2,0]]*4(d-1) /. TensoReductionCoefficients[bubble] //. ScalelessIntegralRules // Factor


ExpandToTensorBasis[triangle] = {
  INT[1,DD[k,m1_] DD[Plus[k,qq__],m2_]DD[Plus[k,pp__],m3_]]:>c0[Plus[pp],-Plus[qq],m1,m2,m3],
  INT[dot[k,mu_],DD[k,m1_] DD[Plus[k,pp__],m2_]DD[Plus[k,qq__],m3_]]:>(
      dot[mu,Plus[pp]]*c1[Plus[pp],-Plus[qq],m1,m2,m3]
    + dot[mu,-Plus[qq]]*c2[Plus[pp],-Plus[qq],m1,m2,m3]
  ) /; !MatchQ[mu,k],
  
  INT[dot[k,mu1_]*dot[k,mu2_],DD[k,m1_] DD[Plus[k,qq__],m2_]*DD[Plus[k,pp__],m3_]]:>(
      dot[mu1,mu2]*c00[Plus[pp],-Plus[qq],m1,m2,m3]
    + dot[mu1,Plus[pp]]*dot[mu2,Plus[pp]]*c11[Plus[pp],-Plus[qq],m1,m2,m3]
    + dot[mu1,-Plus[qq]]*dot[mu2,-Plus[qq]]*c22[Plus[pp],-Plus[qq],m1,m2,m3]
    + (dot[mu1,Plus[pp]]*dot[mu2,-Plus[qq]]+dot[mu2,Plus[pp]]*dot[mu1,-Plus[qq]])/2*c12[Plus[pp],-Plus[qq],m1,m2,m3]
  ) /; !MatchQ[mu1,k]&&!MatchQ[mu2,k],
  
INT[dot[k,mu1_]^2,DD[k,m1_]DD[Plus[k,qq__],m2_] DD[Plus[k,pp__],m3_]]:>(
      dot[mu1,mu1]*c00[Plus[pp],-Plus[qq],m1,m2,m3]
    + dot[mu1,Plus[pp]]*dot[mu1,Plus[pp]]*c11[Plus[pp],-Plus[qq],m1,m2,m3]
    + dot[mu1,-Plus[qq]]*dot[mu1,-Plus[qq]]*c22[Plus[pp],-Plus[qq],m1,m2,m3]
    + (dot[mu1,Plus[pp]]*dot[mu1,-Plus[qq]]+dot[mu1,Plus[pp]]*dot[mu1,-Plus[qq]])/2*c12[Plus[pp],-Plus[qq],m1,m2,m3]
  ) /; !MatchQ[mu1,k],
  
  INT[dot[k,mu1_]*dot[k,mu2_]*dot[k,mu3_],DD[k,m1_]DD[Plus[k,qq__],m2_] DD[Plus[k,pp__],m3_]]:>(
      (dot[mu1,mu2]*dot[mu3,Plus[pp]]+dot[mu2,mu3]*dot[mu1,Plus[pp]]+dot[mu3,mu1]*dot[mu2,Plus[pp]])*c001[Plus[pp],-Plus[qq],m1,m2,m3]
    + (dot[mu1,mu2]*dot[mu3,-Plus[qq]]+dot[mu2,mu3]*dot[mu1,-Plus[qq]]+dot[mu3,mu1]*dot[mu2,-Plus[qq]])*c002[Plus[pp],-Plus[qq],m1,m2,m3]
    + dot[mu1,Plus[pp]]*dot[mu2,Plus[pp]]*dot[mu3,Plus[pp]]*c111[Plus[pp],-Plus[qq],m1,m2,m3]
    + dot[mu1,-Plus[qq]]*dot[mu2,-Plus[qq]]*dot[mu3,-Plus[qq]]*c222[Plus[pp],-Plus[qq],m1,m2,m3]
    + (dot[mu1,Plus[pp]]*dot[mu2,Plus[pp]]*dot[mu3,-Plus[qq]]+dot[mu3,Plus[pp]]*dot[mu1,Plus[pp]]*dot[mu2,-Plus[qq]]+dot[mu2,Plus[pp]]*dot[mu3,Plus[pp]]*dot[mu1,-Plus[qq]])*c112[Plus[pp],-Plus[qq],m1,m2,m3]
    + (dot[mu1,Plus[pp]]*dot[mu2,-Plus[qq]]*dot[mu3,-Plus[qq]]+dot[mu3,Plus[pp]]*dot[mu1,-Plus[qq]]*dot[mu2,-Plus[qq]]+dot[mu2,Plus[pp]]*dot[mu3,-Plus[qq]]*dot[mu1,-Plus[qq]])*c122[Plus[pp],-Plus[qq],m1,m2,m3]
  ) /; !MatchQ[mu1,k]&&!MatchQ[mu2,k]
};


r0sol = {c0->INT[1,DD[k,m1]*DD[k-p2,m2]*DD[k+p1,m3]]};

TensoReductionCoefficients[triangle]= Join[r0sol, r1sol, r2sol, r3sol] /. p1sq->dot[p1,p1] /. p2sq->dot[p2,p2] /. p3sq->dot[p1+p2,p1+p2] /. Rule->xRule /. {
xRule[(f:(c0|c1|c2|c00|c11|c22|c12|c001|c002|c111|c222|c112|c122)),X_]:>xRule[f[xPattern[p1,_],xPattern[p2,_],xPattern[m1,_],xPattern[m2,_],xPattern[m3,_]],X]
} /. xRule->RuleDelayed /. xPattern->Pattern;


INT[dot[k,mu1],DD[k,0]*DD[k+p1,m]*DD[k-p2,m]]
% /. ExpandToTensorBasis[triangle]
% //. TensoReductionCoefficients[triangle];
% //. TensoReductionCoefficients[bubble];
Collect[% //. DotExpandRules,_INT,Factor]


file = OpenWrite["PVreduction-triangle.m"];
WriteString[file,"ExpandToTensorBasis[triangle] = ",InputForm[ExpandToTensorBasis[triangle]],";\n"];
WriteString[file,"TensoReductionCoefficients[triangle] = ",InputForm[TensoReductionCoefficients[triangle]],";\n"];
Close[file];
