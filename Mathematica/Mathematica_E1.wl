(* ::Package:: *)

(* start by defining a function for a generic (inverse) propagator *)
DD[k_,m_] := k^2-m^2


(* define the general tadpole function *)
Tadpole[n_,d_,Delta_] := I/(4*Pi)^(d/2)*(-1)^n*Gamma[n-d/2]/Gamma[n]*Delta^(d/2-n)


(* Recall the Feynman parametrisation for the bubble  *)
(* 1/A/B = \[Integral] da 1/(a*A+(1-a)*B)^2 *)


(* Now check it: *)
Integrate[1/(a*A+(1-a)*B)^2,{a,0,1},Assumptions->Im[A]==0&&Im[B]==0]


Integrate[2*a/(a*A+(1-a)*B)^3,{a,0,1},Assumptions->Im[A]==0&&Im[B]==0]


(* Complete the square to find Delta *)
(1-a)*DD[k,m]+a*DD[k-p,m]
Collect[%,k,f]
{c0, c1, c2} = Coefficient[%%,k,{0,1,2}]
%%% /. k->k-c1/2 // Collect[#,{k,p},Factor]&
Delta = k^2-%


(* Extract the overall mass scale *)
DeltaHat = Collect[Delta/m^2,p,Factor]


Tadpole[2,4-2*eps,Delta] // FullSimplify[#,Assumptions->m>0]&


Normal@Series[DeltaHat^(-eps),{eps,0,1}]
(* You can check that direct integration before changing variables doesn't work *)
(*Collect[%,eps,Integrate[#,{a,0,1}]&]*)


beta[psq_,msq_]:=Sqrt[1-4*msq/psq];
beta[sign_, psq_, msq_]:=1/2*(1+sign*beta[psq,msq])


(* part 1 *)
beta[+1,p^2,m^2]*beta[-1,p^2,m^2] // Simplify


(* part 2 *)
DeltaHat /. p^2->m^2/bp/bm /. a->bp+y // Factor
%*bp*bm /. bp->1/2*(1+bb) /. bm->1/2*(1-bb) // Factor
DeltaHatY = %/bp/bm


(* For reference in case we don't remember how to integrate the logarithm *)
Integrate[Log[y],{y,A,B},Assumptions->Im[A]==0&&Im[B]==0&&A>0&&B>0&&A!=B]


f/@({a,a-1}/. a->bp+y)
Solve[(#==0),y][[1]]&/@({a,a-1}/. a->bp+y)


(* part 3 *)
limits = y /. Solve[(#==0),y][[1]]&/@({a,a-1}/. a->bp+y)

res = Integrate[Log[DeltaHatY],{y,limits[[1]],limits[[2]]}]


(* ::InheritFromParent:: *)
(*ConditionalExpression[-2-bb Log[bb-bp]+bb Log[1+bb-bp]-(-1+bp) Log[((-1+bp) (-1-bb+bp))/(bm bp)]+bp Log[(-bb+bp)/bm],(bp\[NotElement]Reals||Re[bp]>1||Re[bp]<0)&&(-bb+bp\[NotElement]Reals||1+Re[bb]<Re[bp]||Re[bb]>Re[bp])]*)


res[[1]]-(
 -2 + bb*Log[-bp/bm]
 ) /. bm->1/2*(1-bb) /. bp->1/2*(1+bb) // FullSimplify
 % // PowerExpand



