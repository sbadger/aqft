(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


(* ::Subsection:: *)
(*Helper functions*)


SetAttributes[dot,Orderless];


Tadpole[n_,d_,Delta_] := I/(4*Pi)^(d/2)*(-1)^n*Gamma[n-d/2]/Gamma[n]*Delta^(d/2-n)

FeynmanParam[Numerator_, D_List, alpha_List, Powers_List] := Module[{nn,norm,num,den,a,b,c,Delta},

  nn = Plus@@Powers;
  norm = Gamma[nn]/Product[alpha[[ii]]^(Powers[[ii]]-1)/Gamma[Powers[[ii]]],{ii,Range@Length@alpha}];
  den = alpha . D;
  {a,b,c} = Coefficient[den,k,{2,1,0}];
  num = Numerator /. k->k-b/a/2;
  den = Collect[den /. k->k-b/a/2,k,Factor];
  Delta = k^2-den;
  Return[norm*INT[num,nn,d,Delta,Variables[alpha]]];

]

ScalarIntegralRules = {
  INT[1,DD[l1_,m1_]]:>Tadpole[1,d,m1],
  INT[1,DD[l1_,m1_]*DD[l2_,m2_]]:>FeynmanParam[1,{l1^2-m1,l2^2-m2},{1-al[1],al[1]},{1,1}],
  INT[1,n_,d,Delta_,alpha_]:>INT[Tadpole[n,d,Delta],alpha]
};


rGamma = Gamma[1+eps]*(4*Pi)^eps;


(* g[x] indicates gamma matrix, gstr[...] indicates product of gamma matrices *)
ExpandGammaStrings = {
  g[Plus[a_,b__]]:>g[a]+g[Plus[b]],
  g[Times[a_?NumericQ,b_]]:>a*g[b],
  gstr[a1__,Plus[a_,b__],a2__]:>gstr[a1,a,a2]+gstr[a1,Plus[b],a2],
  gstr[a1__,Times[a_?NumericQ,b__],a2__]:>a*gstr[a1,b,a2],
  tr[a1___,Plus[a_,b__],a2___]:>tr[a1,a,a2]+tr[a1,Plus[b],a2],
  tr[a1___,Times[a_?NumericQ,b___],a2___]:>a*tr[a1,b,a2],
  gstr[a1__,mQ,a2__]:>mQ*gstr[a1,a2],
  gstr[a1__,-mQ,a2__]:>-mQ*gstr[a1,a2],
  gstr[a_,g[mu[i_]],x___,y_,g[mu[i_]],b__]:>-gstr[a,g[mu[i]],x,g[mu[i]],y,b]+2*gstr[a,y,x,b],
  gstr[a_,g[mu[i_]],g[mu[i_]],b__]:>d*gstr[a,b],
  
  gstr[aa_,XX__,aa_]:>tr[XX],
  tr[]->4,
  tr[X__] :> 0 /; OddQ[Length[{X}]],
  tr[g[a1_],g[a2_]]:> 4*dot[a1,a2],
  tr[g[a1_],g[a2_],g[a3_],g[a4_]]:>4*(dot[a1,a2]*dot[a3,a4]-dot[a1,a3]*dot[a2,a4]+dot[a1,a4]*dot[a2,a3])
};


ContractRules = {
  dot[a_,b_mu]*dot[b_mu,c_]:>dot[a,c],
  dot[a_,b_mu]^2:>dot[a,a],
  dot[a_mu,a_mu]:>d,
  dot[a_,b_mu]*gstr[c__,g[b_mu],d__]:>gstr[c,g[a],d],
  gstr[x1__,a_]*gstr[a_,x2__]:>gstr[x1,x2]
};


DotExpandRules = {
  dot[Plus[a_,b__],c_]:>dot[a,c]+dot[Plus[b],c],
  dot[Times[n_?NumericQ, a_],b_]:>n*dot[a,b]
};


CollectIntegralRules = {
  INT[n_,x_]*DD[y_,m_]:>INT[n*(dot[y,y]-m),x], 
  INT[n_,x_]*Power[DD[y__],a_]:>INT[n,x/DD[y]^a] /; a<0,
  INT[n_,x_]*Power[DD[y_,m_],a_]:>INT[n*(dot[y,y]-m)^a,x] /; a>0,
  dot[k,x_]*INT[n_,props_]:>INT[n*dot[k,x],props]
};


ScalelessIntegralRules = {
  INT[1,DD[k+p1,0] DD[k+p2,0]]->INT[1,DD[k,0] DD[k-p1+p2,0]], (* integral symmetry *)
  INT[1,DD[k-p1,0] DD[k-p2,0]]->INT[1,DD[k,0] DD[k-p1+p2,0]], (* integral symmetry *)
  INT[1,DD[k+p1,0] DD[k-p2,0]]->INT[1,DD[k,0] DD[k-p1-p2,0]], (* integral symmetry *)
  INT[1,DD[k-p1,0] DD[k+p2,0]]->INT[1,DD[k,0] DD[k-p1-p2,0]], (* integral symmetry *)
  INT[1,DD[k,0] DD[k+p1+p2,0]]->INT[1,DD[k,0] DD[k-p1-p2,0]], (* integral symmetry *)
  INT[1,DD[k,0] DD[k+p1-p2,0]]->INT[1,DD[k,0] DD[k-p1+p2,0]], (* integral symmetry *)
  INT[1,DD[k+p1,mQ] DD[k+p2,mQ]]->INT[1,DD[k,mQ] DD[k-p1+p2,mQ]], (* integral symmetry *)
  INT[1,DD[k-p1,mQ] DD[k-p2,mQ]]->INT[1,DD[k,mQ] DD[k-p1+p2,mQ]], (* integral symmetry *)
  INT[1,DD[k+p1,mQ] DD[k-p2,mQ]]->INT[1,DD[k,mQ] DD[k-p1-p2,mQ]], (* integral symmetry *)
  INT[1,DD[k-p1,mQ] DD[k+p2,mQ]]->INT[1,DD[k,mQ] DD[k-p1-p2,mQ]], (* integral symmetry *)
  INT[1,DD[k,mQ] DD[k+p1+p2,mQ]]->INT[1,DD[k,mQ] DD[k-p1-p2,mQ]], (* integral symmetry *)
  INT[1,DD[k,mQ] DD[k+p1-p2,mQ]]->INT[1,DD[k,mQ] DD[k-p1+p2,mQ]], (* integral symmetry *)
  INT[_,DD[k,0] DD[k-p2,0]]->0,
  INT[_,DD[k,0] DD[k+p1,0]]->0,
  INT[_,DD[k,0] DD[k+p2,0]]->0,
  INT[_,DD[k,0] DD[k-p1,0]]->0,
  INT[_,DD[k,0]]->0,
  (*INT[dot[k,p_],DD[k,m_]]:>0 /; !MatchQ[p,k],*) (* not scaleless but zero due to parity odd integrand *)
  INT[1,1]->0,
  INT[1,DD[k+p2,mQ] DD[k-p1+p2,mQ]]->INT[1,DD[k,mQ] DD[k-p1,mQ]]
};


(* ::Subsection::Closed:: *)
(*Colour algebra*)


colourrules = {
F[a1_ca,a2_ca,a3_ca]:>-2*I*(trT[a1,a2,a3]-trT[a2,a1,a3]),
T[x___,i_cf]*T[i_cf,y___]:>T[x,y],
T[i_cf,x__,i_cf]:>trT[x],
(* various versions of the Fierz identity *)
T[i_cf,a_ca,j_cf]*T[k_cf,a_ca,l_cf]:>1/2*(Delta[i,l]*Delta[k,j]-1/Nc*Delta[i,j]*Delta[k,l]),
T[i1__,a_ca,i2__]*T[i3__,a_ca,i4__]:>1/2*(T[i1,i4]*T[i3,i2]-1/Nc*T[i1,i2]*T[i3,i4]),
T[i1__,a_ca,i2__]*trT[i3___,a_ca,i4___]:>1/2*(T[i1,i4,i3,i2]-1/Nc*T[i1,i2]*trT[i3,i4]),
T[i1__,a_ca,i2___,a_ca,i3___]:>1/2*(T[i1,i3]*trT[i2]-1/Nc*T[i1,i2,i3]),

trT[i1___,a_ca,i2___]*trT[i3___,a_ca,i4___]:>1/2*(trT[i1,i4,i3,i2]-1/Nc*trT[i1,i2]*trT[i3,i4]),
trT[i1___,a_ca,i2___,a_ca,i3___]:>1/2*(trT[i1,i3]*trT[i2]-1/Nc*trT[i1,i2,i3]),

Delta[i_cf,j_cf]*Delta[j_cf,k_cf]:>Delta[i,k],
Delta[i_cf,j_cf]*T[j_cf,x__,k_cf]:>T[i,x,k],
Delta[a_ca,b_ca]*T[x1__,b_ca,x2__]:>T[x1,a,x2],
Delta[a_ca,b_ca]*trT[x1___,b_ca,x2___]:>trT[x1,a,x2],
Delta[b_ca,a_ca]*trT[x1___,b_ca,x2___]:>trT[x1,a,x2],

Delta[i_ca,j_ca]:>Delta[j,i] /; !MatchQ[Sort[{i,j}],{i,j}],
Delta[i_cf,i_cf]:>Nc,
Delta[a_ca,a_ca]:>Nc^2-1,

trT[]:>Nc,
trT[a_ca]:>0,
trT[a_ca,b_ca]:>Delta[a,b]/2,
T[i1_cf,i2_cf]:>Delta[i1,i2]
};


(* example contraction *)
F[ca[1],ca[2],ca[-1]]*F[ca[-1],ca[3],ca[4]]
FixedPoint[Expand[# //. colourrules]&,%];
% /. trT[x1__,ca[1],x2___]:>trT[ca[1],x2,x1]


(* adjoint Casimir *)
F[ca[1],ca[-1],ca[-2]]*F[ca[2],ca[-2],ca[-1]]
FixedPoint[Expand[# //. colourrules]&,%]


(* Jacobi Identity *)
F[ca[1],ca[2],ca[-1]]*F[ca[-1],ca[3],ca[4]]-F[ca[1],ca[3],ca[-1]]*F[ca[-1],ca[2],ca[4]]+F[ca[1],ca[4],ca[-1]]*F[ca[-1],ca[2],ca[3]]
FixedPoint[Expand[# //. colourrules]&,%];
% /. trT[x1__,ca[1],x2___]:>trT[ca[1],x2,x1]


(* gluon 3-vertex test 1 *)
trT[ca[1],ca[-1],ca[-2]]*trT[ca[2],ca[-2],ca[-1]];
FixedPoint[Expand[# //. colourrules]&,%]


(* gluon 3-vertex test 1 *)
trT[ca[1],ca[-1],ca[-2]]*trT[ca[2],ca[-1],ca[-2]];
FixedPoint[Expand[# //. colourrules]&,%]


colourCF = (Nc^2-1)/2/Nc;
colourCA = Nc;


(* ::Subsection::Closed:: *)
(*Load Passarino-Veltman reduction rules*)


Get["PVreduction-bubble.m"];


Get["PVreduction-triangle.m"];


(* ::Subsection::Closed:: *)
(*Feynman rules*)


V3g[p1_,p2_,p3_,mu1_,mu2_,mu3_,a1_,a2_,a3_] := gsR*MuR^eps*F[a1,a2,a3]*(dot[mu1,mu2]*dot[p1-p2,mu3]+dot[mu2,mu3]*dot[p2-p3,mu1]+dot[mu3,mu1]*dot[p3-p1,mu2])

V4g[mu1_,mu2_,mu3_,mu4_,a1_,a2_,a3_,a4_] := I*gsR^2*MuR^(2*eps)*(
    F[a1,a2,ca[-99]]*F[ca[-99],a3,a4]*(dot[mu1,mu3]*dot[mu2,mu4]-dot[mu1,mu4]*dot[mu2,mu3])
  + F[a1,a3,ca[-99]]*F[ca[-99],a2,a4]*(dot[mu1,mu2]*dot[mu3,mu4]-dot[mu1,mu4]*dot[mu2,mu3])
  + F[a1,a4,ca[-99]]*F[ca[-99],a2,a3]*(dot[mu1,mu3]*dot[mu2,mu4]-dot[mu1,mu2]*dot[mu3,mu4])
)

Pg[p_,mu1_,mu2_,a1_,a2_]:= -I*dot[mu1,mu2]*Delta[a1,a2]
Pc[p_,a1_,a2_]:= I*Delta[a1,a2]
Vcgc[p1_,p2_,p3_,mu2_,a1_,a2_,a3_] := gsR*MuR^eps*F[a1,a2,a3]*dot[p3,mu2]

Vqgq[p1_,p2_,p3_,al1_,mu2_,al3_,i1_,a2_,i3_] := -gsR*MuR^eps*I*gstr[al3,g[mu2],al1]*T[i3,a2,i1]
Pq[p_,m_,al1_,al2_,i1_,i2_]:= I*gstr[al2,g[p]+m,al1]*Delta[i2,i1]


(* ::Subsection::Closed:: *)
(*Quark self energy: wave-function counter-terms*)


Num = (
  Vqgq[-p,k,-k+p,aa,mu[-1],al[-2],cf[1],ca[-1],cf[-2]]*
  Pq[-k+p,mQ,al[-2],al[-3],cf[-2],cf[-3]]*
  Vqgq[k-p,p,-k,al[-3],mu[-4],bb,cf[-3],ca[-4],cf[2]]*
  Pg[k,mu[-1],mu[-4],ca[-1],ca[-4]]
);
Num = FixedPoint[Expand[# //. colourrules]&,Num];
Num = Expand[Num] //. ContractRules;
Num = Num //. ExpandGammaStrings //. DotExpandRules;
Num  = Collect[Num /. dot[k,k]->(DD[k,0]),{_gstr,_dot},Factor]


QSEgraph = Expand[Num*INT[1,1]/DD[k,0]/DD[k-p,mQ^2]];
QSEgraph = QSEgraph  /. gstr[a__,g[k],b__]:>dot[k,mu[1]]*gstr[a,g[mu[1]],b] //. CollectIntegralRules;
QSEgraph = Collect[QSEgraph /. ExpandToTensorBasis[bubble] //. ContractRules,_gstr,Factor];
QSEgraph = Collect[QSEgraph /. TensoReductionCoefficients[bubble] /. ScalelessIntegralRules,{_Delta,_gstr,_INT},Factor]


QSEgraph = Collect[QSEgraph //. ScalarIntegralRules /. d->4-2*eps // Simplify,{_INT},Factor];
QSEgraph = QSEgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
(* expand in eps in two steps - since the Gamma[eps] pole has been idenfiified in the integral coefficient we expand the integral to O(eps)  *)
QSEgraph = QSEgraph  /. INT[x_,y_]:>INT[Normal@Series[x,{eps,0,1}],y];
QSEgraph = QSEgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
QSEgraph = Collect[QSEgraph,_INT,Factor];


(* now integrate. use symbols msq and psq and integrate in the Euclidean region where psq<0 *)
(* This is to simplify the expressions and ensure factors of I*Pi do not appear *)
QSEgraphInt = QSEgraph /. dot[p,p]->psq /. p^2->psq /. mQ^2->mQsq /. {
  INT[1,{al[1]}]->Integrate[1,{al[1],0,1}],
  INT[Log[(psq*(-1+al[1])) al[1]],{al[1]}]->Integrate[Log[(psq (-1+al[1])) al[1]],{al[1],0,1},Assumptions->\!\(\*
TagBox[
StyleBox[
RowBox[{"Element", "[", 
RowBox[{"psq", ",", "Reals"}], "]"}],
ShowSpecialCharacters->False,
ShowStringCharacters->True,
NumberMarks->True],
FullForm]\)],
  INT[Log[(mQsq+psq (-1+al[1])) al[1]],{al[1]}]->Integrate[Log[(mQsq+psq (-1+al[1])) al[1]],{al[1],0,1},Assumptions->\!\(\*
TagBox[
StyleBox[
RowBox[{"Element", "[", 
RowBox[{"msq", ",", "Reals"}], "]"}],
ShowSpecialCharacters->False,
ShowStringCharacters->True,
NumberMarks->True],
FullForm]\)&&\!\(\*
TagBox[
StyleBox[
RowBox[{"Element", "[", 
RowBox[{"psq", ",", "Reals"}], "]"}],
ShowSpecialCharacters->False,
ShowStringCharacters->True,
NumberMarks->True],
FullForm]\)&&psq<msq&&msq>0]
};
QSEgraphInt = Normal@Series[QSEgraphInt/rGamma,{eps,0,0}];
QSEgraphInt = Collect[QSEgraphInt,{eps,_gstr},Simplify];
QSEgraphInt = QSEgraphInt /. Log[MuR]->1/2*Log[MuR^2] /. Log[MuR^2]->Log[MuR^2/(mQsq)]+Log[mQsq]/. gsR->Sqrt[\[Alpha]s*4*Pi];
QSEgraphInt = Collect[QSEgraphInt,{eps,_gstr,_Log},Factor]


QSEcounterterm = I*((gstr[bb,g[p],aa]-mQ*gstr[bb,aa])*dpsi-mQ*dmQ*gstr[bb,aa])*Delta[cf[2],cf[1]];


QSEfinite = QSEgraphInt+QSEcounterterm /. {
  dmQ->\[Alpha]s/(4*Pi)*(Nc^2-1)/(2*Nc)*(-3/eps),
  dpsi->\[Alpha]s/(4*Pi)*(Nc^2-1)/(2*Nc)*(-1/eps)
};
Collect[QSEfinite/colourCF/(\[Alpha]s/(4*Pi)/(-I)),{eps,_gstr},Factor]


(* ::Subsection::Closed:: *)
(*Gluon vacuum polarisation: gluon wave-function counter-term*)


Num1 = nf*(
  Vqgq[p-k,-p,k,al[-1],mu[1],al[-2],cf[-1],ca[1],cf[-2]]*
  Pq[k,mQ,al[-2],al[-3],cf[-2],cf[-3]]*
  Vqgq[-k,p,-p+k,al[-3],mu[2],al[-4],cf[-3],ca[2],cf[-4]]*
  Pq[-p+k,mQ,al[-4],al[-1],cf[-4],cf[-1]]
);
Num1 = FixedPoint[Expand[# //. colourrules]&,Num1];
Num1 = Expand[Num1] //. ContractRules;
Num1 = Num1 //. ExpandGammaStrings //. DotExpandRules;
Num1  = Collect[Num1 /. dot[k,k]->(DD[k,mQ^2]+mQ^2),_dot,Factor]


VPgraph1 = Expand[Num1*INT[1,1]/DD[k,mQ^2]/DD[k-p,mQ^2]];
VPgraph1 = VPgraph1 //. CollectIntegralRules;
VPgraph1 = Collect[VPgraph1,_INT,Factor]


Num2 = Dg*1/2*(
  V3g[-p,k,p-k,mu[1],mu[-2],mu[-1],ca[1],ca[-2],ca[-1]]*
  Pg[k,mu[-2],mu[-3],ca[-2],ca[-3]]*
  V3g[p,-p+k,-k,mu[2],mu[-4],mu[-3],ca[2],ca[-3],ca[-4]]*
  Pg[-p+k,mu[-4],mu[-1],ca[-4],ca[-1]]
);
Num2 = FixedPoint[Expand[# //. colourrules]&,Num2];
Num2 = Expand[Num2] //. ContractRules;
Num2 = Num2 //. ExpandGammaStrings //. DotExpandRules;
Num2  = Collect[Num2 /. dot[k,k]->(DD[k,0]),_dot,Factor]


Num3 = -Dc*(
  Vcgc[p-k,-p,k,mu[1],ca[-1],ca[1],ca[-2]]*
  Pc[k,ca[-2],ca[-3]]*
  Vcgc[-k,p,-p+k,mu[2],ca[-4],ca[2],ca[-3]]*
  Pc[-p+k,ca[-4],ca[-1]]
);
Num3 = FixedPoint[Expand[# //. colourrules]&,Num3];
Num3 = Expand[Num3] //. ContractRules;
Num3 = Num3 //. ExpandGammaStrings //. DotExpandRules;
Num3  = Collect[Num3 /. dot[k,k]->(DD[k,0]),_dot,Factor]


VPgraph2 = Expand[(Num2+Num3)*INT[1,1]/DD[k,0]/DD[k-p,0]];
VPgraph2 = VPgraph2 //. CollectIntegralRules;
VPgraph2 = Collect[VPgraph2,{_INT,_Delta,_dot},Factor]


VPgraph = VPgraph1+VPgraph2;
VPgraph = Collect[VPgraph /. ExpandToTensorBasis[bubble] //. ContractRules,_gstr,Factor];
VPgraphtmp = Collect[VPgraph /. TensoReductionCoefficients[bubble] /. INT[1,DD[k-p,mm_]]:>INT[1,DD[k,mm]] /. ScalelessIntegralRules,{_INT,_dot},Factor]
VPgraph = Collect[VPgraphtmp //. ScalarIntegralRules /. d->4-2*eps // Simplify,{_INT},Factor];
VPgraph = VPgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
(* expand in eps in two steps - since the Gamma[eps] pole has been idenfiified in the integral coefficient we expand the integral to O(eps)  *)
VPgraph = VPgraph /. INT[x_,y_]:>INT[Normal@Series[x,{eps,0,1}],y];
VPgraph = VPgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
VPgraph = Collect[VPgraph,_INT,Factor]


Collect[Coefficient[VPgraphtmp,Dg],{_INT,_dot},Factor]
Collect[Coefficient[VPgraphtmp,Dc],{_INT,_dot},Factor]


(* now integrate. use symbols msq and psq and integrate in the Euclidean region where psq<0 *)
(* This is to simplify the expressions and ensure factors of I*Pi do not appear *)
VPgraphInt = VPgraph /. Dg->1 /. Dc->1  /. p^2->psq /. dot[p,p]->psq /. mQ^2->mQsq //. {
  INT[Log[mQsq+psq (-1+al[1]) al[1]],{al[1]}]->INT[Log[1+psq/mQsq (-1+al[1]) al[1]],{al[1]}]+INT[1,{al[1]}]*Log[mQsq],
  INT[1,{al[1]}]->Integrate[1,{al[1],0,1}],
  INT[Log[1+psq/mQsq (-1+al[1]) al[1]],{al[1]}]->-2+beta[psq,mQsq]*Log[-beta[1,psq,mQsq]/beta[-1,psq,mQsq]],
  INT[Log[(psq*(-1+al[1])) al[1]],{al[1]}] -> Integrate[Log[(psq (-1+al[1])) al[1]],{al[1],0,1},Assumptions->\!\(\*
TagBox[
StyleBox[
RowBox[{"Element", "[", 
RowBox[{"psq", ",", "Reals"}], "]"}],
ShowSpecialCharacters->False,
ShowStringCharacters->True,
NumberMarks->True],
FullForm]\)]
};
VPgraphInt = Normal@Series[VPgraphInt/rGamma,{eps,0,-1}];
VPgraphInt = Collect[VPgraphInt,{eps,_dot},Simplify];
VPgraphInt = VPgraphInt /. Log[MuR]->1/2*Log[MuR^2] /. Log[MuR^2]->Log[MuR^2/(mQsq)]+Log[mQsq] /. gsR->Sqrt[\[Alpha]s*4*Pi];
VPgraphInt = Collect[Factor[VPgraphInt/(-psq*dot[mu[1],mu[2]]+dot[p,mu[1]]*dot[p,mu[2]])],{eps,_dot,_Log},Factor]


VPcounterterm = -I*(-psq*dot[mu[1],mu[2]]+dot[p,mu[1]]*dot[p,mu[2]])*Delta[ca[1],ca[2]]*dA


Coefficient[Coefficient[VPgraphInt,eps,-1],nf,1]/\[Alpha]s*4*Pi
Coefficient[Coefficient[VPgraphInt,eps,-1],nf,0]/\[Alpha]s*4*Pi


VPfinite = VPgraphInt + VPcounterterm/(-psq*dot[mu[1],mu[2]]+dot[p,mu[1]]*dot[p,mu[2]]) /. {
  dA->\[Alpha]s/(4*Pi)*((5*Nc-2*nf)/3/eps)
};
VPfinite = Collect[VPfinite/(\[Alpha]s/(4*Pi)/(-I)),{eps,_gstr,_Log},Factor]


(* ::Subsection:: *)
(*QQG Vertex correction: vertex counter-term, \[Delta]1*)


CliffordRules = {
  gstr[a_,g[m_mu],x___,y_,g[m_mu],b__]:>-gstr[a,g[m],x,g[m],y,b]+2*gstr[a,y,x,b],
  gstr[a__,g[x_],g[x_],b__]:>dot[x,x]*gstr[a,b],
  gstr[vb2,x___,g[y_],g[p2],b__]:>-gstr[vb2,x,g[p2],g[y],b]+2*dot[p2,y]*gstr[vb2,x,b],
  gstr[vb2,x___,g[y_],g[k],b__]:>-gstr[vb2,x,g[k],g[y],b]+2*dot[k,y]*gstr[vb2,x,b] /; !MatchQ[y,p2],
  gstr[b__,g[p1],g[y_],x___,u1]:>-gstr[b,g[y],g[p1],x,u1]+2*dot[p1,y]*gstr[b,x,u1],
  gstr[x__,g[p1],u1]:>mQ*gstr[x,u1],
  gstr[vb2,g[p2],x__]:>-mQ*gstr[vb2,x]
};


Num1 = (
  gstr[vb2,al[2]]*gstr[al[1],u1]*
  Vqgq[-p1,k,-k+p1,al[1],mu[-1],al[-2],cf[1],ca[-1],cf[-2]]*
  Pq[-k+p1,mQ,al[-2],al[-3],cf[-2],cf[-3]]*
  Vqgq[-p1+k,p1-p2,p2-k,al[-3],mu[3],al[-4],cf[-3],ca[3],cf[-4]]*
  Pq[p2-k,mQ,al[-4],al[-5],cf[-4],cf[-5]]*
  Vqgq[k-p2,-k,p2,al[-5],mu[-2],al[2],cf[-5],ca[-2],cf[2]]*
  Pg[k,mu[-1],mu[-2],ca[-1],ca[-2]]
);
Num1 = FixedPoint[Expand[# //. colourrules]&,Num1];
Num1 = Expand[Num1] //. ContractRules;
Num1 = Num1 //. ExpandGammaStrings //. DotExpandRules;
Num1 = Num1 //. CliffordRules;
Num1  = Collect[Num1 /. dot[k,k]->(DD[k,0]),_gstr,Factor]


VRTgraph1 = Expand[Num1*INT[1,1]/DD[k,0]/DD[k-p1,mQ^2]/DD[k-p2,mQ^2]];


Num2 = (
  gstr[vb2,al[2]]*gstr[al[1],u1]*
  Vqgq[-p1,p1-k,k,al[1],mu[-1],al[-2],cf[1],ca[-1],cf[-2]]*
  Pq[k,mQ,al[-2],al[-3],cf[-2],cf[-3]]*
  Vqgq[-k,-p2+k,p2,al[-3],mu[-4],al[2],cf[-3],ca[-4],cf[2]]*
  Pg[p1-k,mu[-1],mu[-2],ca[-1],ca[-2]]*
  V3g[k-p1,p1-p2,p2-k,mu[-2],mu[3],mu[-3],ca[-2],ca[3],ca[-3]]*
  Pg[p2-k,mu[-3],mu[-4],ca[-3],ca[-4]]
);
Num2 = FixedPoint[Expand[# //. colourrules]&,Num2];
Num2 = Expand[Num2] //. ContractRules;
Num2 = Num2 //. ExpandGammaStrings //. DotExpandRules;
Num2 = Num2 //. CliffordRules;
Num2  = Collect[Num2 /. dot[k,k]->(DD[k,mQ^2]+mQ^2),_gstr,Factor]


VRTgraph2 = Expand[Num2*INT[1,1]/DD[k,mQ^2]/DD[k-p1,0]/DD[k-p2,0]];


VRTgraph = VRTgraph1+VRTgraph2 /. gstr[vb2,g[k],u1]->gstr[vb2,g[mu[-1]],u1]*dot[mu[-1],k] //. CollectIntegralRules;
VRTgraph = Collect[VRTgraph,{D1,D2,_INT},Factor];

VRTgraph = Collect[Expand[VRTgraph /. ExpandToTensorBasis[triangle]]//. DotExpandRules //. ContractRules //. DotExpandRules,_gstr,Factor];
VRTgraph = Collect[VRTgraph //. TensoReductionCoefficients[triangle] //. TensoReductionCoefficients[bubble] //. ScalelessIntegralRules,{_INT},Factor];
VRTgraph = VRTgraph //. CliffordRules //. DotExpandRules /. a0[0]->0 /. a0[mQ^2]->INT[1,DD[k,mQ^2]];
VRTgraph = VRTgraph /. dot[p1,p1]->mQ^2 /. dot[p2,p2]->mQ^2;
VRTgraph = VRTgraph /. dot[p1,mu[3]]->dot[p2,mu[3]]+1/gstr[vb2,u1]*(2*mQ*gstr[vb2,g[mu[3]],u1]-I*gstr[vb2,sig[mu[3],p12],u1]);
Collect[VRTgraph,{_INT},Factor]


VRTgraph = Collect[VRTgraph //. ScalarIntegralRules /. d->4-2*eps// Simplify,{_INT},Factor]  /. (p1-p2)^2->Qsq;
VRTgraph = VRTgraph /. INT[n_,args__]:>INT[Simplify[n],args];
VRTgraph = VRTgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
(* expand in eps in two steps - since the Gamma[eps] pole has been idenfiified in the integral coefficient we expand the integral to O(eps)  *)
VRTgraph = VRTgraph  /. INT[x_,y_]:>INT[Normal@Series[x,{eps,0,1}],y];
VRTgraph = VRTgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
VRTgraph = Collect[VRTgraph,_INT,Factor];


(* now integrate. use symbols msq and psq and integrate in the Euclidean region where psq<0 *)
(* This is to simplify the expressions and ensure factors of I*Pi do not appear *)
VRTgraphInt = VRTgraph /. Q^2->Qsq //. {
  INT[1,{al[1]}]->Integrate[1,{al[1],0,1}],
  INT[Log[(Qsq*(-1+al[1])) al[1]],{al[1]}] -> Integrate[Log[(Qsq (-1+al[1])) al[1]],{al[1],0,1},Assumptions->\!\(\*
TagBox[
StyleBox[
RowBox[{"Element", "[", 
RowBox[{"Qsq", ",", "Reals"}], "]"}],
ShowSpecialCharacters->False,
ShowStringCharacters->True,
NumberMarks->True],
FullForm]\)]
};
VRTgraphInt = Normal@Series[VRTgraphInt/rGamma,{eps,0,-1}];
VRTgraphInt = Collect[VRTgraphInt,{eps,_gstr},Simplify];
VRTgraphInt = Collect[VRTgraphInt /. Log[MuR]->1/2*Log[MuR^2] /. Log[MuR^2]->Log[MuRsq/(-Qsq)]+Log[-Qsq] /. gsR->Sqrt[\[Alpha]s*4*Pi],{eps,_gstr,_Log,_INT},Factor]


VRTcounterterm = -I*gsR*MuR^eps*gstr[vb2,g[mu[3]],u1]*T[cf[2],ca[3],cf[1]]*d1 /. gsR->Sqrt[\[Alpha]s*4*Pi]


VRTgraphInt+VRTcounterterm /. d1->\[Alpha]s/(4*Pi)*(-1/eps*(colourCF+colourCA));
VRTfinite = Collect[Normal@Series[%/(-I*Sqrt[\[Alpha]s*4*Pi]*MuR^eps),{eps,0,-1}],{eps,_gstr,_Log,_INT},Factor];
VRTfinite = Collect[VRTfinite //. {
Log[MuR]->(Log[MuRsq/-Qsq]+Log[-Qsq])/2,
dot[p1,p2]->-(Qsq-2*mQ^2)/2,
INT[1,DD[k,0] DD[k-p1,mQ^2] DD[k-p2,mQ^2]]->-I/(4*Pi)^2*I3hat[Qsq,mQ^2]
},{eps,_gstr,_Log,_I3hat,_beta},Factor]


dgs = d1-dpsi-1/2*dA /. {
  d1->\[Alpha]s/(4*Pi)*(-1/eps*(colourCF+colourCA)),
  dmQ->\[Alpha]s/(4*Pi)*colourCF*(-3/eps),
  dpsi->\[Alpha]s/(4*Pi)*colourCF*(-1/eps),
  dA->\[Alpha]s/(4*Pi)*((5*colourCA-2*nf)/3/eps)
} // Factor


(* ::Subsection::Closed:: *)
(*3G Vertex correction: vertex counter-term, \[Delta]13g*)


CliffordRules = {
  gstr[a_,g[m_mu],x___,y_,g[m_mu],b__]:>-gstr[a,g[m],x,g[m],y,b]+2*gstr[a,y,x,b],
  gstr[a__,g[x_],g[x_],b__]:>dot[x,x]*gstr[a,b],
  tr[a___,g[m_mu],x___,y_,g[m_mu],b___]:>-tr[a,g[m],x,g[m],y,b]+2*tr[a,y,x,b],
  tr[a___,g[x_],g[x_],b___]:>dot[x,x]*tr[a,b],
  tr[a___,g[y_],g[k],b___]:>-tr[a,g[k],g[y],b]+2*dot[k,y]*tr[a,b],
  gstr[vb2,x___,g[y_],g[p2],b__]:>-gstr[vb2,x,g[p2],g[y],b]+2*dot[p2,y]*gstr[vb2,x,b],
  gstr[vb2,x___,g[y_],g[k],b__]:>-gstr[vb2,x,g[k],g[y],b]+2*dot[k,y]*gstr[vb2,x,b] /; !MatchQ[y,p2],
  gstr[b__,g[p1],g[y_],x___,u1]:>-gstr[b,g[y],g[p1],x,u1]+2*dot[p1,y]*gstr[b,x,u1],
  gstr[x__,g[p1],u1]:>mQ*gstr[x,u1],
  gstr[vb2,g[p2],x__]:>-mQ*gstr[vb2,x],
  tr[x__]:>0 /; OddQ[Length[{x}]],
  tr[]->4,
  tr[g[p1_],g[p2_]]:>4*dot[p1,p2],
  tr[g[p1_],g[p2_],g[p3_],g[p4_]]:>4*(dot[p1,p2]*dot[p3,p4]-dot[p1,p3]*dot[p2,p4]+dot[p1,p4]*dot[p2,p3]),
  tr[g[p1_],g[p2_],g[p3_],g[p4_],g[p5_],g[p6_]]:>4*(
    + dot[p1,p2]*tr[g[p3,g[p4],g[p5],g[p6]]]
    - dot[p1,p3]*tr[g[p2,g[p4],g[p5],g[p6]]]
    + dot[p1,p4]*tr[g[p2,g[p3],g[p5],g[p6]]]
    - dot[p1,p5]*tr[g[p2,g[p3],g[p4],g[p6]]]
    + dot[p1,p6]*tr[g[p2,g[p3],g[p4],g[p5]]]
    )
};


Num1 = (
  D1*V3g[-k+p1,-p1,k,mu[-1],mu[1],mu[-2],ca[-1],ca[1],ca[-2]]*
  Pg[k,mu[-2],mu[-3],ca[-2],ca[-3]]*
  V3g[-k,p1-p2,k+p2-p1,mu[-3],mu[3],mu[-4],ca[-3],ca[3],ca[-4]]*
  Pg[k+p2-p1,mu[-4],mu[-5],ca[-4],ca[-5]]*
  V3g[-k-p2+p1,p2,k-p1,mu[-5],mu[2],mu[-6],ca[-5],ca[2],ca[-6]]*
  Pg[k-p1,mu[-6],mu[-1],ca[-6],ca[-1]]
);
Num1 = FixedPoint[Expand[# //. colourrules]&,Num1];
Num1 = FixedPoint[Expand[#] //. ContractRules&,Num1];
Num1 = Num1 //. ExpandGammaStrings //. DotExpandRules;
Num1 = Num1 //. CliffordRules;
Num1  = Collect[Num1 /. dot[k,k]->(DD[k,0]),_gstr,Factor]


Num2 = (
  D2*Vcgc[-k+p1,-p1,k,mu[1],ca[-1],ca[1],ca[-2]]*
  Pc[k,ca[-2],ca[-3]]*
  Vcgc[-k,p1-p2,k+p2-p1,mu[3],ca[-3],ca[3],ca[-4]]*
  Pc[k+p2-p1,ca[-4],ca[-5]]*
  Vcgc[-k-p2+p1,p2,k-p1,mu[2],ca[-5],ca[2],ca[-6]]*
  Pc[k-p1,ca[-6],ca[-1]]
);
Num2 = FixedPoint[Expand[# //. colourrules]&,Num2];
Num2 = FixedPoint[Expand[#] //. ContractRules&,Num2];
Num2 = Num2 //. ExpandGammaStrings //. DotExpandRules;
Num2 = Num2 //. CliffordRules;
Num2  = Collect[Num2 /. dot[k,k]->(DD[k,0]),_gstr,Factor]


Num2q = (
  nf*Dq1*Vqgq[-k+p1,-p1,k,al[-1],mu[1],al[-2],cf[-1],ca[1],cf[-2]]*
  Pq[k,mQ,al[-2],al[-3],cf[-2],cf[-3]]*
  Vqgq[-k,p1-p2,k+p2-p1,al[-3],mu[3],al[-4],cf[-3],ca[3],cf[-4]]*
  Pq[k+p2-p1,mQ,al[-4],al[-5],cf[-4],cf[-5]]*
  Vqgq[-k-p2+p1,p2,k-p1,al[-5],mu[2],al[-6],cf[-5],ca[2],cf[-6]]*
  Pq[k-p1,mQ,al[-6],al[-1],cf[-6],cf[-1]]
);
Num2q = FixedPoint[Expand[# //. colourrules]&,Num2q];
Num2q = FixedPoint[Expand[#] //. ContractRules&,Num2q];
Num2q = Num2q //. ExpandGammaStrings //. DotExpandRules;
Num2q = Num2q //. CliffordRules;
Num2q  = Collect[Num2q /. dot[k,k]->(DD[k,mQ^2]+mQ^2),_gstr,Factor]


Num3 = (
  D3*V4g[mu[-4],mu[2],mu[1],mu[-1],ca[-4],ca[2],ca[1],ca[-1]]
  Pg[k,mu[-1],mu[-2],ca[-1],ca[-2]]*
  V3g[-k,p1-p2,k+p2-p1,mu[-2],mu[3],mu[-3],ca[-2],ca[3],ca[-3]]*
  Pg[k+p2-p1,mu[-3],mu[-4],ca[-3],ca[-4]]
);
Num3 = FixedPoint[Expand[# //. colourrules]&,Num3];
Num3 = FixedPoint[Expand[#] //. ContractRules&,Num3];
Num3 = Num3 //. ExpandGammaStrings //. DotExpandRules;
Num3 = Num3 //. CliffordRules;
Num3  = Collect[Num3 /. dot[k,k]->(DD[k,0]),_gstr,Factor]


Num4 = (
  D4*Vcgc[-k-p2,p2,k,mu[2],ca[-1],ca[2],ca[-2]]*
  Pc[k,ca[-2],ca[-3]]*
  Vcgc[-k,-p2+p1,k-p1+p2,mu[3],ca[-3],ca[3],ca[-4]]*
  Pc[k-p1+p2,ca[-4],ca[-5]]*
  Vcgc[-k-p1+p2,p1,k+p1,mu[1],ca[-5],ca[1],ca[-6]]*
  Pc[k+p2,ca[-6],ca[-1]]
);
Num4 = FixedPoint[Expand[# //. colourrules]&,Num4];
Num4 = FixedPoint[Expand[#] //. ContractRules&,Num4];
Num4 = Num4 //. ExpandGammaStrings //. DotExpandRules;
Num4 = Num4 //. CliffordRules;
Num4  = Collect[Num4 /. dot[k,k]->(DD[k,0]),_gstr,Factor]


Num4q = (
  nf*Dq2*Vqgq[-k-p2,p2,k,al[-1],mu[2],al[-2],cf[-1],ca[2],cf[-2]]*
  Pq[k,mQ,al[-2],al[-3],cf[-2],cf[-3]]*
  Vqgq[-k,-p2+p1,k-p1+p2,al[-3],mu[3],al[-4],cf[-3],ca[3],cf[-4]]*
  Pq[k-p1+p2,mQ,al[-4],al[-5],cf[-4],cf[-5]]*
  Vqgq[-k-p1+p2,p1,k+p1,al[-5],mu[1],al[-6],cf[-5],ca[1],cf[-6]]*
  Pq[k+p2,mQ,al[-6],al[-1],cf[-6],cf[-1]]
);
Num4q = FixedPoint[Expand[# //. colourrules]&,Num4q];
Num4q = FixedPoint[Expand[#] //. ContractRules&,Num4q];
Num4q = Num4q //. ExpandGammaStrings //. DotExpandRules;
Num4q = Num4q //. CliffordRules;
Num4q  = Collect[Num4q /. dot[k,k]->(DD[k,mQ^2]+mQ^2),_gstr,Factor]


VRTgraph1 = Expand[(Num1+Num2)*INT[1,1]/DD[k,0]/DD[k-p1,0]/DD[k+p2-p1,0]];
VRTgraph2 = Expand[Num3*INT[1,1]/DD[k,0]/DD[k+p2-p1,0]];
VRTgraph3 = Expand[Num4*INT[1,1]/DD[k,0]/DD[k+p2,0]/DD[k-p1+p2,0]];

VRTgraph4 = Expand[Num2q*INT[1,1]/DD[k,mQ^2]/DD[k-p1,mQ^2]/DD[k+p2-p1,mQ^2]];
VRTgraph5 = Expand[Num4q*INT[1,1]/DD[k,mQ^2]/DD[k+p2,mQ^2]/DD[k-p1+p2,mQ^2]];


VRTgraph = VRTgraph1+VRTgraph2+VRTgraph3+VRTgraph4+VRTgraph5 //. CollectIntegralRules;
VRTgraph = Collect[VRTgraph,{_INT},Factor];
VRTgraph = VRTgraph /. {
   INT[n_,DD[k+p2,mQ^2] DD[k-p1+p2,mQ^2]] :> INT[n /. k->k-p2,DD[k,mQ^2] DD[k-p1,mQ^2]],
   INT[n_,DD[k-p1,mQ^2] DD[k-p1+p2,mQ^2]] :> INT[n /. k->k+p1,DD[k,mQ^2] DD[k+p2,mQ^2]]
} //. DotExpandRules;
VRTgraph = VRTgraph /. {
  INT[dot[k,m_]+dot[p1,m_],x_]:>INT[dot[k,m],x]+dot[p1,m]*INT[1,x],
  INT[dot[k,m_]-dot[p2,m_],x_]:>INT[dot[k,m],x]-dot[p2,m]*INT[1,x]
}

(*VRTgraph = Collect[Expand[VRTgraph /. ExpandToTensorBasis[triangle]]//. DotExpandRules //. ContractRules //. DotExpandRules,_gstr,Factor];
VRTgraph = Collect[Expand[VRTgraph /. ExpandToTensorBasis[bubble]]//. DotExpandRules //. ContractRules //. DotExpandRules,_gstr,Factor];
VRTgraph = Collect[VRTgraph //. TensoReductionCoefficients[triangle] //. TensoReductionCoefficients[bubble] //. ScalelessIntegralRules,{_INT}];
VRTgraph = VRTgraph //. CliffordRules //. DotExpandRules /. a0[0]->0 /. a0[mQ^2]->INT[1,DD[k,mQ^2]];
VRTgraph = VRTgraph /. dot[p1,p1]->psq /. dot[p2,p2]->psq /. INT[1,DD[k,mQ^2] DD[k+p2,mQ^2]]->INT[1,DD[k,mQ^2] DD[k-p1,mQ^2]];
VRTgraph = Collect[VRTgraph,{_INT,nf},Factor];*)


VRTgraph // Variables


VRTgraph = Collect[VRTgraph //. ScalarIntegralRules /. d->4-2*eps,{_INT}];
VRTgraph = VRTgraph /. INT[n_,args__]:>INT[Simplify[n],args];
VRTgraph = VRTgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
(* expand in eps in two steps - since the Gamma[eps] pole has been idenfiified in the integral coefficient we expand the integral to O(eps)  *)
VRTgraph = VRTgraph  /. INT[x_,y_]:>INT[Normal@Series[x,{eps,0,0}],y];
VRTgraph = VRTgraph //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_, a_],b_]:>n*INT[a,b] /; And@@(FreeQ[n,#]&/@b)};
VRTgraph = Collect[VRTgraph,_INT,Factor];


VRTgraph // Variables


(* now integrate. use symbols msq and psq and integrate in the Euclidean region where psq<0 *)
(* This is to simplify the expressions and ensure factors of I*Pi do not appear *)
VRTgraphInt = VRTgraph //. {
  INT[1,{al[1]}]->Integrate[1,{al[1],0,1}],
  INT[Log[(Qsq*(-1+al[1])) al[1]],{al[1]}] -> Integrate[Log[(Qsq*(-1+al[1]))*al[1]],{al[1],0,1},Assumptions->\!\(\*
TagBox[
StyleBox[
RowBox[{"Element", "[", 
RowBox[{"Qsq", ",", "Reals"}], "]"}],
ShowSpecialCharacters->False,
ShowStringCharacters->True,
NumberMarks->True],
FullForm]\)],
  INT[Log[1+Qsq/mQsq (-1+al[1]) al[1]],{al[1]}]->-2+beta[Qsq,mQsq]*Log[-beta[1,Qsq,mQsq]/beta[-1,Qsq,mQsq]]
};
VRTgraphInt = Normal@Series[VRTgraphInt/rGamma,{eps,0,-1}];
VRTgraphInt = VRTgraphInt //. DotExpandRules /. trT[x__,ca[1],y___]:>trT[ca[1],y,x];
VRTgraphInt = Collect[VRTgraphInt,{eps,_gstr},Simplify];
VRTgraphInt = Collect[VRTgraphInt /. Log[MuR]->1/2*Log[MuR^2] /. Log[MuR^2]->Log[MuRsq/(-Qsq)]+Log[-Qsq] /. gsR->Sqrt[\[Alpha]s*4*Pi],{eps,_Log,_INT},Factor]


VRTgraphInt /. p2->p1+dd //. DotExpandRules;
% /. dot[p1,p1]->psq /. dot[dd,x_]:>dd*dot[1,x];
VRTgraphInt = Normal@Series[%,{dd,0,0}] // Factor


VRT3gcounterterm = gsR*MuR^eps*F[ca[1],ca[2],ca[3]]*(dot[mu[1],mu[2]]*dot[p1-p2,mu[3]]+dot[mu[2],mu[3]]*dot[p2-p3,mu[1]]+dot[mu[3],mu[1]]*dot[p3-p1,mu[2]]) /. gsR->Sqrt[\[Alpha]s*4*Pi];
VRT3gcounterterm = VRT3gcounterterm /. p1->-p1 /. p3->p1-p2 /. p2->p1  /. DotExpandRules  /. dot[0,_]:>0;
VRT3gcounterterm = FixedPoint[Expand[# //. colourrules]&,VRT3gcounterterm*d13g] /. trT[x__,ca[1],y___]:>trT[ca[1],y,x]  // Factor


VRTgraphInt+VRT3gcounterterm/. Dq1->1 /. Dq2->1 /. d13g->\[Alpha]s/(4*Pi)*(2/3*nf/eps);
VRTfinite = Collect[Normal@Series[%/(-I*Sqrt[\[Alpha]s*4*Pi]*MuR^eps),{eps,0,-1}],{eps,_gstr,_Log,_INT},Factor];
VRTfinite = Collect[VRTfinite //. {
},{eps,_gstr,_dot},Factor]


dgs = d1-dpsi-1/2*dA /. {
  d1->\[Alpha]s/(4*Pi)*(-1/eps*(colourCF+colourCA)),
  dmQ->\[Alpha]s/(4*Pi)*colourCF*(-3/eps),
  dpsi->\[Alpha]s/(4*Pi)*colourCF*(-1/eps),
  dA->\[Alpha]s/(4*Pi)*((5*colourCA-2*nf)/3/eps)
} // Factor


dgs = d13g-3/2*dA /. {
  d13g->\[Alpha]s/(4*Pi)*(2/3*nf/eps),
  dmQ->\[Alpha]s/(4*Pi)*colourCF*(-3/eps),
  dpsi->\[Alpha]s/(4*Pi)*colourCF*(-1/eps),
  dA->\[Alpha]s/(4*Pi)*((5*colourCA-2*nf)/3/eps)
} // Factor


(* ::Subsection:: *)
(*test*)


INT[dot[k,mu[1]] dot[k,mu[2]],DD[k,0] DD[k-p2,0] DD[k+p1,0]]
% /. ExpandToTensorBasis[triangle]//. DotExpandRules //. ContractRules //. DotExpandRules
% //. TensoReductionCoefficients[triangle] //. TensoReductionCoefficients[bubble] //. ScalelessIntegralRules;
% /. dot[p1,p1]->0 /. dot[p2,p2]->0


(*VRTgraph = Collect[Expand[VRTgraph /. ExpandToTensorBasis[triangle]]//. DotExpandRules //. ContractRules //. DotExpandRules,_gstr,Factor];
VRTgraph = Collect[Expand[VRTgraph /. ExpandToTensorBasis[bubble]]//. DotExpandRules //. ContractRules //. DotExpandRules,_gstr,Factor];
VRTgraph = Collect[VRTgraph //. TensoReductionCoefficients[triangle] //. TensoReductionCoefficients[bubble] //. ScalelessIntegralRules,{_INT}];


Num1 = Expand[Num1] //. ContractRules;
Num1 = Num1 //. ExpandGammaStrings //. DotExpandRules;
Num1 = Num1 //. CliffordRules;
Num1  = Collect[Num1 /. dot[k,k]->(DD[k,0]),_gstr,Factor]


gstr[ub1,g[mu[-1]],g[k+p1],g[mu],g[k-p2],g[mu[-1]],v2]
% //. CliffordRules //. ExpandGammaStrings //. DotExpandRules //. ContractRules;
% //. CliffordRules;
% /. gstr[__,g[p2],v2]:>0 /. gstr[ub1,g[p1],__]:>0 /. dot[k,k]->0


Vqeq[p1_,p2_,p3_,al1_,mu2_,al3_,i1_,i3_] := -geR*MuR^eps*I*gstr[al3,g[mu2],al1]*Delta[i3,i1]


Num1 = (
  gstr[vb2,al[2]]*gstr[al[1],u1]*
  Vqgq[-p1,k,-k+p1,al[1],mu[-1],al[-2],cf[1],ca[-1],cf[-2]]*
  Pq[-k+p1,0,al[-2],al[-3],cf[-2],cf[-3]]*
  Vqeq[-p1+k,p1-p2,p2-k,al[-3],mu[3],al[-4],cf[-3],cf[-4]]*
  Pq[p2-k,0,al[-4],al[-5],cf[-4],cf[-5]]*
  Vqgq[k-p2,-k,p2,al[-5],mu[-2],al[2],cf[-5],ca[-2],cf[2]]*
  Pg[k,mu[-1],mu[-2],ca[-1],ca[-2]]
);
Num1 = FixedPoint[Expand[# //. colourrules]&,Num1];
Num1 = Expand[Num1] //. ContractRules;
Num1 = Num1 //. ExpandGammaStrings //. DotExpandRules;
Num1 = Num1 //. CliffordRules;
Num1  = Collect[Num1 /. dot[k,k]->(DD[k,0]) /. mQ->0,_gstr,Factor]


VRTgraph1 = Expand[Num1*INT[1,1]/DD[k,0]/DD[k-p1,0]/DD[k-p2,0]];


VRTgraph = VRTgraph1 /. gstr[vb2,g[k],u1]->gstr[vb2,g[mu[-1]],u1]*dot[mu[-1],k] //. CollectIntegralRules;
VRTgraph = Collect[VRTgraph,{D1,D2,_INT},Factor];

VRTgraph = Collect[Expand[VRTgraph /. ExpandToTensorBasis[triangle]]//. DotExpandRules //. ContractRules //. DotExpandRules,_gstr,Factor];
VRTgraph = Collect[VRTgraph //. TensoReductionCoefficients[triangle] //. TensoReductionCoefficients[bubble] //. ScalelessIntegralRules,{_INT},Factor];
VRTgraph = VRTgraph //. CliffordRules //. DotExpandRules /. a0[0]->0;
VRTgraph = VRTgraph /. dot[p1,p1]->0 /. dot[p2,p2]->0 /. mQ->0;
Collect[VRTgraph,{_INT},Factor]
% /. dot[p1,p2]->s12/2 // Factor



