(* ::Package:: *)

(* Rank one and two bubble tensor reduction problem *)


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


(*

Representation of lorentz indices, vectors and momenta

g^{mu,nu} = dot[mu,nu]
p^mu = dot[p,mu]
p1.p2 = dot[p1,p2]

*)

SetAttributes[dot,Orderless];

ContractRules = {
  dot[a_,b_mu]*dot[b_mu,c_]:>dot[a,c],
  dot[a_,b_mu]^2:>dot[a,a],
  dot[a_mu,a_mu]:>d
};

DotExpandRules = {
  dot[Plus[a_,b__],c_]:>dot[a,c]+dot[Plus[b],c],
  dot[Times[n_?NumericQ, a_],b_]:>n*dot[a,b]
};


Num[mu1_] := dot[k,mu1]
TensorBasis[mu1_,p_] := {dot[p,mu1]}

Num[mu1_,mu2_] := dot[k,mu1]*dot[k,mu2]
TensorBasis[mu1_,mu2_,p_] := {dot[mu1,mu2],dot[p,mu1]*dot[p,mu2]}


(* rewrite dot products as propagators *)
ToPropagators = {dot[k,p]->1/2(DD[k,m1]-DD[k-p,m2]+dot[p,p]+m1-m2), dot[k,k]->DD[k,m1]+m1}


Basis = TensorBasis[mu[1],p];
FormFactors = {b1};
Integrand = Num[mu[1]]/DD[k,m1]/DD[k-p,m2]

(* calculate the contractions with the tensor integrals in terms of scalar integrals *)
Expand[Integrand*Basis] //. ContractRules /. ToPropagators
(* now collect into integrals, expand dot products in numerator and reduce tadpoles *)
Expand[%*INT[1,1]];
% //. {
  INT[n_,x_]*DD[y_,m_]:>INT[n*(dot[y,y]-m),x],
  INT[n_,x_]*Power[DD[y_,m_],a_]:>INT[n,x/DD[y,m]^a] /; a<0,
  INT[n_,x_]*Power[DD[y_,m_],a_]:>INT[n*(dot[y,y]-m)^2,x] /; a>0
 };
% /. INT[n_,DD[k-p,mm_]]:>INT[n /. k->k+p,DD[k,mm]];
(*assign as Left Hand Side (LHS) of the equations*)
LHS = % /. INT[1,1]->0

(* now the Right Hand Side (RHS) of the equations  *)
RHS = Outer[Times,Basis,Basis] . FormFactors //. ContractRules

(* now construct equations and solve linear system *)
eqs = Equal@@@Transpose@{LHS,RHS}
r1sol = Solve[eqs,FormFactors][[1]] /. Rule[a_,b_]:>Rule[a,Collect[b,_INT,Simplify]]


Basis = TensorBasis[mu[1],mu[2],p];
FormFactors = {b00,b11};
Integrand = Num[mu[1],mu[2]]/DD[k,m1]/DD[k-p,m2]

(* calculate the contractions with the tensor integrals in terms of scalar integrals *)
Expand[Integrand*Basis] //. ContractRules /. ToPropagators
(* now collect into integrals, expand dot products in numerator and reduce tadpoles *)
Expand[%*INT[1,1]];
% //. {INT[n_,x_]*DD[y_,m_]:>INT[n*(dot[y,y]-m),x], INT[n_,x_]*Power[DD[y__],a_]:>INT[n,x/DD[y]^a] /; a<0, INT[n_,x_]*Power[DD[y_,m_],a_]:>INT[n*(dot[y,y]-m)^2,x] /; a>0};
% /. INT[n_,DD[k-p,mm_]]:>INT[n /. k->k+p,DD[k,mm]];
% /. INT[n_,props_]:>INT[n  //. DotExpandRules,props];
% //. {INT[Plus[a_,b__],c_]:>INT[a,c]+INT[Plus[b],c], INT[Times[n_?NumericQ, a_],b_]:>n*INT[a,b], INT[n_,b_]:>n*INT[1,b] /; FreeQ[n,k]&&!MatchQ[n,1]};
(* tadpole tensor reductions then assign as Left Hand Side (LHS) of the equations*)
LHS = % /. {INT[dot[k,k],DD[k,mm_]]:>INT[1,1]+mm*INT[1,DD[k,mm]], INT[dot[k,p],DD[k,mm_]]->0} /. INT[1,1]->0

(* now the Right Hand Side (RHS) of the equations  *)
RHS = Outer[Times,Basis,Basis] . FormFactors //. ContractRules

(* now construct equations and solve linear system *)
eqs = Equal@@@Transpose@{LHS,RHS}
r2sol = Solve[eqs,FormFactors][[1]] /. Rule[a_,b_]:>Rule[a,Collect[b,_INT,Simplify]]


(* show exmaples for rank 1 *)
r1sol /. {m1->0, m2->0}
r1sol /. {m1->0, m2->m} /. INT[1,DD[k,0]]->0
r1sol /. {m1->m, m2->m}


(* show exmaples for rank 2 *)
r2sol /. {m1->0, m2->0}
r2sol /. {m1->0, m2->m} /. INT[1,DD[k,0]]->0
r2sol /. {m1->m, m2->m}


ExpandToTensorBasis[bubble]={
  INT[dot[k,mu_],DD[k,m1_]*DD[Plus[k,p__],m2_]] :> dot[-Plus[p],mu]*b1[-Plus[p],m1,m2] /; !MatchQ[mu,k],
  INT[dot[k,mu_]*dot[k,nu_],DD[k,m1_]*DD[Plus[k,p__],m2_]] :> dot[mu,nu]*b00[-Plus[p],m1,m2]+dot[-Plus[p],mu]*dot[-Plus[p],nu]*b11[-Plus[p],m1,m2] /; !MatchQ[mu,k]&&!MatchQ[nu,k],
  INT[dot[k,mu_]^2,DD[k,m1_]*DD[Plus[k,p__],m2_]] :> dot[mu,mu]*b00[-Plus[p],m1,m2]+dot[-Plus[p],mu]^2*b11[-Plus[p],m1,m2] /; !MatchQ[mu,k]
}


r0sol = {b0->INT[1,DD[k,m1] DD[k-p,m2]]};

TensoReductionCoefficients[bubble]= Join[r0sol, r1sol, r2sol] /. Rule->xRule /. {
(f:(b0|b1|b00|b11)):>f[xPattern[p,_],xPattern[m1,_],xPattern[m2,_]]
} /. xRule->RuleDelayed /. xPattern->Pattern


INT[dot[k,mu[1]],DD[k,0]*DD[k+q,m]]
% /. ExpandToTensorBasis[bubble]
% /. TensoReductionCoefficients[bubble]


file = OpenWrite["PVreduction-bubble.m"];
WriteString[file,"ExpandToTensorBasis[bubble] = ",InputForm[ExpandToTensorBasis[bubble]],";\n"];
WriteString[file,"TensoReductionCoefficients[bubble] = ",InputForm[TensoReductionCoefficients[bubble]],";\n"];
Close[file];
